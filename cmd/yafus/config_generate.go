package main

import (
	"fmt"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.com/DerEnderKeks/yafus/internal/config"
	"gopkg.in/yaml.v3"
	"os"
)

func init() {
	configCmd.AddCommand(configGenerateCmd)
}

var configGenerateCmd = &cobra.Command{
	Use:    "generate",
	Short:  "Generate a sample config",
	Run:    ConfigGenerateCmdRun,
	PreRun: ConfigGenerateCmdPreRun,
}

func ConfigGenerateCmdPreRun(cmd *cobra.Command, args []string) {
	validateConfig()
}

func ConfigGenerateCmdRun(cmd *cobra.Command, args []string) {
	c, err := config.GenerateDefault()
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to generate config")
	}

	out, err := yaml.Marshal(c)

	if err != nil {
		log.Fatal().Err(err).Msg("Failed to marshal config")
	}

	_, err = fmt.Fprintln(os.Stdout, string(out))
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to print config")
	}
}
