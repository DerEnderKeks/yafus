package main

import (
	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(manageCmd)
}

var manageCmd = &cobra.Command{
	Use:   "manage",
	Short: "Management commands",
}
