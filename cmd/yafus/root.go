package main

import (
	"github.com/cristalhq/aconfig"
	"github.com/cristalhq/aconfig/aconfigyaml"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.com/DerEnderKeks/yafus/internal/config"
	"io"
	"os"
	"os/signal"
	"runtime/pprof"
	"strings"
	"time"
)

var rootCmd = &cobra.Command{
	Use:              "yafus",
	Short:            "yet another file upload server",
	PersistentPreRun: RootCmdPreRun,
}

func initLogging(verboseCount *int) func() {
	return func() {
		logLevel, err := zerolog.ParseLevel(strings.ToLower(yConfig.General.LogLevel))
		if err != nil {
			log.Warn().Err(err).Msg("Failed to parse log level, defaulting to INFO")
			logLevel = zerolog.InfoLevel
		}

		if *verboseCount > 0 {
			logLevel -= zerolog.Level(*verboseCount)
			if logLevel < zerolog.TraceLevel {
				logLevel = zerolog.TraceLevel
			}

			log.Debug().Str("level", logLevel.String()).Msg("Log level adjusted by verbose flag")
		}

		var consoleOutput io.Writer
		switch strings.ToLower(yConfig.General.LogFormat) {
		case "json":
			zerolog.TimeFieldFormat = time.RFC3339
			consoleOutput = os.Stderr
		default:
			consoleOutput = zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: time.RFC3339}
		}

		log.Logger = zerolog.New(consoleOutput).Level(logLevel).With().Timestamp().Logger()
	}
}

func initConfig(configPath *string) func() {
	return func() {
		yamlDecoder := aconfigyaml.New()
		aConfigConfig := aconfig.Config{
			AllowDuplicates:    false,
			AllowUnknownFields: false,
			AllowUnknownFlags:  false,
			AllowUnknownEnvs:   false,
			FailOnFileNotFound: false,
			SkipFlags:          true,
			EnvPrefix:          "YAFUS",
			Files:              []string{"/etc/yafus/config.yml", "/etc/yafus/config.yaml"},
			FileDecoders: map[string]aconfig.FileDecoder{
				".yml":  yamlDecoder,
				".yaml": yamlDecoder,
			},
		}

		// TODO check required fields depending execution context, remove generic requirements

		if *configPath != "" {
			// if specified, use it exclusively and fail when not found
			aConfigConfig.FailOnFileNotFound = true
			aConfigConfig.Files = []string{*configPath}
		}

		loader := aconfig.LoaderFor(yConfig, aConfigConfig)

		if err := loader.Load(); err != nil {
			log.Fatal().Err(err).Msg("Failed to load config")
		}
	}
}

func initProfiling(cpuprofile *string) func() {
	return func() {
		if *cpuprofile != "" {
			log.Info().Str("path", *cpuprofile).Msg("Starting CPU profiling")
			f, err := os.Create(*cpuprofile)
			if err != nil {
				log.Fatal().Err(err).Msg("Failed to create CPU profiling file")
			}
			err = pprof.StartCPUProfile(f)
			if err != nil {
				log.Fatal().Err(err).Msg("Failed to start CPU profiling")
			}
		}
	}
}

var yConfig *config.Root
var requiredConfigSections []string

func validateConfig() {
	err := yConfig.Validate(requiredConfigSections...)
	if err != nil {
		log.Fatal().Err(err).Msg("Config validation failed")
	}
}

func requireConfigSections(s ...config.Section) {
	for _, a := range s {
		requiredConfigSections = append(requiredConfigSections, a.StructPath())
	}
}

func init() {
	var verboseCount int
	var configPath string
	var cpuprofile string

	yConfig = &config.Root{}

	cobra.OnInitialize(initConfig(&configPath), initLogging(&verboseCount), initProfiling(&cpuprofile))

	rootCmd.PersistentFlags().CountVarP(&verboseCount, "verbose", "v", "increase logging verbosity, can be used multiple times")
	rootCmd.PersistentFlags().StringVarP(&configPath, "config", "c", "", "specify path to config file")
	rootCmd.PersistentFlags().StringVar(&cpuprofile, "cpuprofile", "", "enable cpu profiling and store it at the specified path")
	_ = rootCmd.PersistentFlags().MarkHidden("cpuprofile")
}

func RootCmdPreRun(cmd *cobra.Command, args []string) {
	requireConfigSections(config.GeneralConfig{})
}

func Execute() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	// TODO pass to cobra
	go func() {
		for range c {
			pprof.StopCPUProfile()
			os.Exit(0)
		}
	}()
	_ = rootCmd.Execute()
}
