package main

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"reflect"
	"testing"
)

func Test_initLogging(t *testing.T) {
	t.Run("logger updated", func(t *testing.T) {
		level := 0
		oldLogger := log.Logger
		initLogging(&level)()
		if reflect.DeepEqual(oldLogger, log.Logger) {
			t.Errorf("logger didn't change")
		}
	})

	levelTests := []struct {
		name string
		arg  int
		want zerolog.Level
	}{
		{name: "no change", arg: 0, want: zerolog.InfoLevel},
		{name: "debug", arg: 1, want: zerolog.DebugLevel},
		{name: "trace", arg: 2, want: zerolog.TraceLevel},
		{name: "beyond trace", arg: 99, want: zerolog.TraceLevel},
	}
	for _, tt := range levelTests {
		t.Run(tt.name, func(t *testing.T) {
			initLogging(&tt.arg)()
			if log.Logger.GetLevel() != tt.want {
				t.Errorf("unexpected log level: got %v, want %v", log.Logger.GetLevel(), tt.want)
			}
		})
	}
}
