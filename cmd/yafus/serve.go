package main

import (
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.com/DerEnderKeks/yafus/internal/auth"
	"gitlab.com/DerEnderKeks/yafus/internal/config"
	"gitlab.com/DerEnderKeks/yafus/internal/server"
	"gitlab.com/DerEnderKeks/yafus/internal/store"
)

func init() {
	rootCmd.AddCommand(serveCmd)
}

var serveCmd = &cobra.Command{
	Use:    "serve",
	Short:  "Start the web server",
	Run:    ServeCmdRun,
	PreRun: ServeCmdPreRun,
}

func ServeCmdPreRun(cmd *cobra.Command, args []string) {
	requireConfigSections(config.AuthConfig{}, config.DatabaseConfig{}, config.ServerConfig{}, config.StorageConfig{})
	validateConfig()
}

func ServeCmdRun(cmd *cobra.Command, args []string) {
	dataStore, err := store.NewStore(yConfig.Database, yConfig.Storage)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to init data store")
	}

	log.Info().Uint("port", yConfig.Server.Port).Uint("trustedProxyCount", yConfig.Server.TrustedProxyCount).Msg("Starting http server")
	s, err := server.NewServer(yConfig.Server.Port, yConfig.Server.TrustedProxyCount, dataStore, auth.OIDCConfig(yConfig.Auth.OIDC))
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to setup http server")
	}

	err = s.Start()
	if err != nil {
		log.Error().Err(err).Msg("Server failed")
	}
}
