package main

import (
	"github.com/spf13/cobra"
	"gitlab.com/DerEnderKeks/yafus/internal/config"
)

func init() {
	manageCmd.AddCommand(manageImportCmd)
}

var manageImportCmd = &cobra.Command{
	Use:              "import",
	Short:            "Import commands",
	PersistentPreRun: ManageImportCmdPersistentPreRun,
}

func ManageImportCmdPersistentPreRun(cmd *cobra.Command, args []string) {
	requireConfigSections(config.DatabaseConfig{}, config.StorageConfig{})
}
