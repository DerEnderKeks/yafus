package main

import (
	"encoding/json"
	"fmt"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v3"
	"os"
)

func init() {
	configDumpCmd.Flags().Bool("json", false, "Dump as JSON (instead of YAML)")

	configCmd.AddCommand(configDumpCmd)
}

var configDumpCmd = &cobra.Command{
	Use:    "dump",
	Short:  "Dump the loaded config",
	Run:    ConfigDumpCmdRun,
	PreRun: ConfigDumpCmdPreRun,
}

func ConfigDumpCmdPreRun(cmd *cobra.Command, args []string) {
	validateConfig()
}

func ConfigDumpCmdRun(cmd *cobra.Command, args []string) {
	dumpAsJSON, _ := cmd.Flags().GetBool("json")

	var out []byte
	var err error
	if dumpAsJSON {
		out, err = json.MarshalIndent(yConfig, "", "  ")
	} else {
		out, err = yaml.Marshal(yConfig)
	}

	if err != nil {
		log.Fatal().Err(err).Msg("Failed to marshal config")
	}

	_, err = fmt.Fprintln(os.Stdout, string(out))
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to print config")
	}
}
