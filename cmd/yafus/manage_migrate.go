package main

import (
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.com/DerEnderKeks/yafus/internal/config"
	"gitlab.com/DerEnderKeks/yafus/internal/store"
)

func init() {
	manageMigrateCmd.Flags().Int64("version", store.MigrateLatestVersion, "target a specific version (-1 = latest)")
	manageMigrateCmd.Flags().Bool("down", false, "undo migrations down to specified version")

	manageCmd.AddCommand(manageMigrateCmd)
}

var manageMigrateCmd = &cobra.Command{
	Use:    "migrate",
	Short:  "Migrate the database",
	Run:    ManageMigrateCmdRun,
	PreRun: ManageMigrateCmdPreRun,
}

func ManageMigrateCmdPreRun(cmd *cobra.Command, args []string) {
	if down, _ := cmd.Flags().GetBool("down"); down {
		_ = cmd.MarkFlagRequired("version")
	}

	requireConfigSections(config.DatabaseConfig{}, config.StorageConfig{})
	validateConfig()
}

func ManageMigrateCmdRun(cmd *cobra.Command, args []string) {
	database, err := store.NewStore(yConfig.Database, yConfig.Storage)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to init db connection")
	}

	version, _ := cmd.LocalFlags().GetInt64("version")
	if err != nil {
		version = store.MigrateLatestVersion
	}

	if down, _ := cmd.Flags().GetBool("down"); down {
		err = database.MigrateDownTo(version)
	} else {
		err = database.MigrateUpTo(version)
	}

	if err != nil {
		log.Fatal().Err(err).Msg("Migration failed")
		return
	}

	log.Info().Int64("version", version).Msg("Migration successful") // TODO
}
