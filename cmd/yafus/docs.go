package main

import (
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/cobra/doc"
	"os"
	"strings"
)

func init() {
	rootCmd.AddCommand(docsCmd)
}

var docsCmd = &cobra.Command{
	Use:    "docs [output-path]",
	Short:  "Generate CLI documentation. Wipes the output directory first.",
	Hidden: true,
	Run:    DocsCmdRun,
}

func DocsCmdRun(cmd *cobra.Command, args []string) {
	outPath := "./docs/cli"
	if len(args) > 0 {
		outPath = args[0]
	}

	dir, err := os.ReadDir(outPath)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to read output directory")
	}
	for _, entry := range dir {
		if entry.IsDir() || !strings.HasSuffix(entry.Name(), ".md") {
			log.Fatal().Msg("Output directory contains files or directories that are not markdown files. Refusing to continue.")
		}
	}

	err = os.RemoveAll(outPath)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to clean up output directory")
	}

	err = os.MkdirAll(outPath, os.ModePerm)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to create output directory")
	}

	err = doc.GenMarkdownTree(rootCmd, outPath)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to generate documentation")
	}

	err = os.Link(outPath+"/yafus.md", outPath+"/Readme.md")
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to create Readme.md symlink")
	}

	log.Info().Msg("Documentation generated")
}
