package main

import (
	"context"
	"encoding/json"
	"github.com/99designs/gqlgen/graphql"
	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.com/DerEnderKeks/yafus/internal/store"
	"gopkg.in/yaml.v3"
	"os"
	"path"
	"time"
)

func init() {
	manageImportCmd.AddCommand(manageImportYaniusCmd)
}

var manageImportYaniusCmd = &cobra.Command{
	Use:    "yanius <path>",
	Short:  "Import Yanius data",
	Args:   cobra.ExactArgs(1),
	Run:    ManageImportYaniusCmdRun,
	PreRun: ManageImportYaniusCmdPreRun,
}

func ManageImportYaniusCmdPreRun(cmd *cobra.Command, args []string) {
	validateConfig()
}

func ManageImportYaniusCmdRun(cmd *cobra.Command, args []string) {
	// import dir structure: db/*.json, files/*, usermap.yml

	type yaniusUser struct {
		ID       string `json:"id"`
		Username string `json:"username"`
	}

	type yaniusFile struct {
		FileID    string `json:"fileId"`
		FileName  string `json:"originalName"`
		UserID    string `json:"uploaderId"`
		PublicID  string `json:"shortName"`
		Timestamp struct {
			Time     float64 `json:"epoch_time"`
			Timezone string  `json:"timezone"`
		} `json:"timestamp"`
	}

	var yaniusUsers []yaniusUser
	var yaniusFiles []yaniusFile

	jsonFiles := map[string]any{
		"users": &yaniusUsers,
		"files": &yaniusFiles,
	}
	for k, v := range jsonFiles {
		filePath := path.Join(args[0], "db", k+".json")
		fileData, err := os.ReadFile(filePath)
		if err != nil {
			log.Fatal().Err(err).Str("path", filePath).Msg("Failed to read file")
		}

		err = json.Unmarshal(fileData, v)
		if err != nil {
			log.Fatal().Err(err).Str("path", filePath).Msg("Failed to unmarshal file")
		}
	}

	yaniusSubMap := map[string]string{} // yanius uuid -> openid sub
	yaniusSubMapPath := path.Join(args[0], "usermap.yml")
	yaniusSubMapData, err := os.ReadFile(yaniusSubMapPath)
	if err != nil {
		log.Fatal().Err(err).Str("path", yaniusSubMapPath).Msg("Failed to read file")
	}

	err = yaml.Unmarshal(yaniusSubMapData, yaniusSubMap)
	if err != nil {
		log.Fatal().Err(err).Str("path", yaniusSubMapPath).Msg("Failed to unmarshal file")
	}

	database, err := store.NewStore(yConfig.Database, yConfig.Storage)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to init db connection")
	}

	ctx := context.Background()

	yaniusUUIDMap := map[string]uuid.UUID{} // yanius uuid -> new uuid

	log.Info().Msg("Starting import")

	importedUsers := 0
	for yaniusUUID, sub := range yaniusSubMap {
		user, err := database.GetOrCreateUserBySub(ctx, sub, func() (*string, error) {
			return nil, nil // name left unset
		})
		if err != nil {
			log.Fatal().Err(err).Msg("Failed to create user")
		}
		importedUsers++
		yaniusUUIDMap[yaniusUUID] = user.ID
	}

	for _, user := range yaniusUsers {
		if _, ok := yaniusUUIDMap[user.ID]; !ok {
			log.Warn().Str("uuid", user.ID).Str("username", user.Username).Msg("Skipping user missing in usermap.yml")
		}
	}

	importedFiles := 0
	for _, fileData := range yaniusFiles {
		userID, ok := yaniusUUIDMap[fileData.UserID]
		if !ok {
			log.Debug().Str("yaniusUserID", fileData.UserID).Str("publicID", fileData.PublicID).Msg("Skipping file for missing user")
			continue
		}

		fPath := path.Join(args[0], "files", fileData.FileID)
		f, err := os.Open(fPath)
		if err != nil {
			log.Error().Err(err).Str("path", fPath).Msg("Failed to open file")
			continue
		}

		stat, err := f.Stat()
		if err != nil {
			log.Error().Err(err).Str("path", fPath).Msg("Failed to stat file")
			continue
		}

		createdAt := time.UnixMilli(int64(fileData.Timestamp.Time * 1e3)) // assuming UTC
		mockUpload := graphql.Upload{
			File:        f,
			Filename:    fileData.FileName,
			Size:        stat.Size(),
			ContentType: "",
		}
		_, err = database.CreateFileWithTimeAndUserIDAndPublicID(ctx, mockUpload, &createdAt, userID, &fileData.PublicID)
		if err != nil {
			log.Error().Err(err).Str("path", fPath).Msg("Failed to create file")
			continue
		}
		importedFiles++
	}

	log.Info().Int("users", importedUsers).Int("files", importedFiles).Msg("Import completed")
}
