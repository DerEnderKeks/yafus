# Architecture

Yafus is a relatively simple Golang application, but requires some external components to function.

[[_TOC_]]

## Structure

```mermaid
flowchart TD
	User --> Yafus
	User --> OIDC
    Yafus --> DB(PostgreSQL)
	Yafus --> S3("S3 (optional)")
	Yafus <--> OIDC(OIDC Provider)
```

Files are either store in a local file system, or in S3. Even when stored in S3, the user always goes through Yafus, which proxies the requests to the S3 provider.

If the S3 storage provider is used, Yafus can run [highly available](Installation.md#high-availability), as Yafus itself is completely stateless.

Except for the static dashboard resources, all interactions with Yafus happen via the [GraphQL API](API.md), thus the API can do everything the UI can do, and more.
