# User Guide

This guide provides instructions for basic usage of the UI and API to upload and share files.

[[_TOC_]]

## Accessing the UI

Navigate to the domain of your Yafus instance. You'll automatically be redirected to `/dashboard`, which will redirect you to your OIDC provider. After authenticating, you'll be back on the dashboard.

Most pages in the UI use the same table structure, where **right-clicking opens a context menu for the clicked item**.

![Screenshot of dashboard](media/dashboard_files.png)

### Files

Here you find a list of all your files, where you can access or delete them. To upload a file, simply click the `Upload` button, where you can drag&drop a file or manually select one. 

### Tokens

Tokens allow you to create a static authentication credential to use the API, most likely for uploading files.

The scope of a token is only configurable during creation. To upload files, only `self.write` is required. The token secret is only visible once.

### Users

Only accessible with the `admin.read` scope. Shows a list of all users and their used storage.

## Uploading files using the API

You can use most HTTP-capable tools to upload files. While the API uses GraphQL (see [API](API.md)), uploading a file is effectively just a multipart form POST request with some JSON.

### General

Request:

* Request URL: `https://<your yafus domain>/graph/query`
* Method: `POST`
* Headers:
  * `Authorization`: `Bearer <token>` 
    * Where `<token>` is a static token created using the UI
  * `Content-Type`: `multipart/form-data;` 
    * Probably set by the tool you are using
* Form fields:
  * `operations`: `{ "query": "mutation ($file: Upload!) { createFile(file: $file) {id, publicID, extension} }", "variables": { "file": null } }` 
  * `map`: `{ "0": ["variables.file"] }` 
  * `0`
    * The file you want to upload

Response:

If successful, the status code is 200. Otherwise, the request failed.

```json
{
  "data": {
    "createFile": {
      "id": "<uuid>",
      "publicID": "<short ID>",
      "extension": "<original extension>",
      "__typename": "File"
    }
  }
}
```

This example doesn't include all available response fields, only the ones set in the `operations` GraphQL query above (after `createFile()`). See [API](API.md#schema) for more information.

### ShareX

Change the domain (twice) and token in the JSON snippet below, and import it into ShareX as a custom uploader.

```json
{
  "Version": "15.0.0",
  "DestinationType": "ImageUploader, TextUploader, FileUploader",
  "RequestMethod": "POST",
  "RequestURL": "https://<your yafus domain>/graph/query",
  "Headers": {
    "Authorization": "Bearer <token>"
  },
  "Body": "MultipartFormData",
  "Arguments": {
    "operations": "\\{ \"query\": \"mutation ($file: Upload!) \\{ createFile(file: $file) \\{ id, publicID, extension \\} \\}\", \"variables\": \\{ \"file\": null \\} \\}",
    "map": "\\{ \"0\": [\"variables.file\"] \\}"
  },
  "FileFormName": "0",
  "URL": "https://<your yafus domain>/{json:data.createFile.publicID}.{json:data.createFile.extension}"
}
```
