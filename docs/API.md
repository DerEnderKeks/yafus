# API

Yafus mainly provides a [GraphQL](https://graphql.org) API, authenticated with OpenID/OAuth2. Additionally, a few other endpoints exist. 

[[_TOC_]]

## Endpoints

* `/` Redirects to `/dashboard/`
* `/dashboard/` Serves the static frontend
* `/health` Reports the status of the server, intended for health checks
* `/<id>[.extension]` Serves the file with the specified ID. The extension is ignored.
  * `Content-Security-Policy` is set to ensure safe display of HTML files
  * `Content-Disposition` is always set to `inline` (with the original file name), making the browser choose whether to download the file or display it directly
* `/graph/query` GraphQL endpoint. Requires authentication.
* `/auth/oidc-config` Returns details about the configured OIDC provider.


## GraphQL

The main API of Yafus, available at `/graph/query`. Always requires authentication.

### Schema

See [schema.graphqls](../internal/server/graph/schema.graphqls).

### Authentication

There are two ways to authenticate: OpenID/OAuth2 and static tokens.

In any case, the token has to be set as `Bearer` token in the `Authorization` header:

```
Authorization: Bearer <token>
```

#### OpenID/OAuth2

This is the primary authentication method, also used by the UI (which uses the Authorization Code Flow with PKCE).

An OAuth2 Access Token can be obtained using various flows, how exactly you obtain it is up to you. The API validates the following:

* Signature (using the JWKS endpoint of the configured OIDC provider)
* Timestamps
* Scopes (see [Installation](Installation.md#openid-backend))
* Existence of user via UserInfo endpoint

#### Static tokens

These can be created using the API and thus the UI. They don't expire and have a static scope assignment.
