# Installation

While the only official installation method currently provided is a container image, you could just compile and run the binary directly (the image contains nothing else either).

This page mostly described the details about the container image, for configuration details see [Configuration](Configuration.md).

[[_TOC_]]

## Versioning

Yafus follows [semantic versioning](https://semver.org) (`<Major>.<Minor>.<Patch>`), meaning that starting with version 1.0.0:

* Unless the major version changes, no breaking changes ~~*should*~~ will occur
* Minor releases contain new features
* Patch releases only contain bug fixes

> ⚠️ It's still recommended to read the changelog for every version, but especially when the major version changes.

> ⚠️ As long as the major version is still `0`, these rules are loosely offset by one number, meaning the minor version might include breaking changes.

## General

The server:

* Provides an HTTP endpoint for both the API and the UI (which uses the API)
  * HTTPS needs to be provided by a reverse proxy. Using plain HTTP is not supported.
* Requires a database as a backend
* Is configured using a configuration file
* Stores files in the local file system or S3, depending on the configuration
* Uses an OpenID compliant backend for user authentication and authorization

Setting up the database, S3 and OpenID backend are out of scope for this documentation. See the following sections for recommendations.

### Database

Only PostgreSQL is supported. Postgres-compatible DBs probably work, but have not been tested so far. 

Verified: 

* Vanilla [PostgreSQL](https://postgresql.org) 15 (other versions probably also work, report any issues with newer versions)

A single database and user is required. 
The user needs full access to the database. 
The schema is created and updated automatically.

[pgx](https://github.com/jackc/pgx) is used as the underlying driver, thus most but not all connection string features are supported.

### S3

Any S3-compliant backend should work. 

Verified:

* [Minio](https://www.minio.io)

A single private bucket is required. Versioning is not recommended.

### OpenID Backend

Any OpenID-compliant backend should work.

Verified:
* [Keycloak](https://www.keycloak.org)

A couple of resources are required:

* At least one client
  * Public (no client credentials)
  * Authorization Code Flow with PKCE (mandatory)
  * Redirect URL and Origin: `https://<your yafus domain>/dashboard/*`
* Scopes (with the configured prefix):
  * admin.read
  * admin.write
  * admin.delete
  * self.read
  * self.write
  * self.delete

Scope access should be restricted by the OIDC backend, as the scopes are currently used for authorization. This will probably change in the future.

## High availability

Yafus fully supports being deployed highly available with multiple replicas, if the following requirements are met:

* S3 storage provider is used. The local storage provider is not intended to be used concurrently, even if it might work with cluster file systems, and thus not supported. 
* A load balancer is used to distribute requests to the Yafus replicas.

Any other components required by Yafus are obviously recommended to also be deployed highly available.

Upgrading the database schema is currently not specifically optimized for having different versions access the database at the same time. Because of that major version updates might cause short downtimes when requests reach backends running an older version.

## Installation methods

### Container

The image is available as:
```
registry.gitlab.com/derenderkeks/yafus:<tag>
```

Using this image requires basic Container deployment knowledge. The ~~*right™️*~~ recommended way is to use Kubernetes, but ~~*more primitive*~~ simpler setups will also work just fine.

#### Tags

**Recommended**: Use the specific tag of the latest stable version (`vX.Y.Z`). If you're into surprise updates, you might also just use the `stable` tag.

The following tagged images are created:

* `v<X>.<Y>.<Z>`: Normal tagged version, only "stable" releases.
* `v<X>.<Y>.<Z>-<special version>`: Special tagged version, like pre-releases.
* `stable`: The latest stable version.
* `latest`: The latest tagged version (might point to pre-releases). Not recommended for production use.
* `branch-<branch>`: Built for every commit in a branch. Not recommended for production use

#### Ports

The server only listens on a single port, the HTTP endpoint, by default `8080` (dual-stack).

#### Mounts

You need to at least mount the configuration file and optionally a volume for the uploaded files.

* `/etc/yafus/config.yml` Configuration file, required. See [Configuration](Configuration.md) for details.
* `/var/lib/yafus/files` Volume for uploaded files, only required when the local storage provider is used (instead of S3).

> ⚠️ If you don't mount a volume, while using the local storage provider, you'll lose your files.

#### Healthcheck

The server provides a `/health` endpoint. If you're using Kubernetes, you can point the readiness-/liveness-/startupProbe at it.

```yaml
readinessProbe:
  httpGet:
    port: http
    path: /health
livenessProbe:
  httpGet:
    port: http
    path: /health
startupProbe:
  httpGet:
    port: http
    path: /health
```
