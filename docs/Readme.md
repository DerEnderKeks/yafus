# Docs

## General

* [Installation](Installation.md)
* [Configuration](Configuration.md)
* [User Guide](User%20Guide.md)

## Advanced

* [Architecture](Architecture.md)
* [API](API.md)
* [CLI](CLI.md)
* [Migration from Yanius](Migration%20from%20Yanius.md)
