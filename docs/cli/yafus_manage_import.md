## yafus manage import

Import commands

### Options

```
  -h, --help   help for import
```

### Options inherited from parent commands

```
  -c, --config string   specify path to config file
  -v, --verbose count   increase logging verbosity, can be used multiple times
```

### SEE ALSO

* [yafus manage](yafus_manage.md)	 - Management commands
* [yafus manage import yanius](yafus_manage_import_yanius.md)	 - Import Yanius data

###### Auto generated by spf13/cobra on 15-Dec-2024
