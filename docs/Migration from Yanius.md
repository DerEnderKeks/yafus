# Migration from Yanius

If you were previously using Yanius, the following steps allow you to import your data into Yafus. These steps can also be used to import files from other systems.

### 1. Preparations

Create a directory accessible on the system running Yafus (or any other system that can access the file storage and database) with the following structure:

```
/some/path
├── db (step 2)
│   └── *.json
├── files (step 3)
│   └── *
└── usermap.yml (step 4)
```

### 2. Export the RethinkDB database

Somehow dump the database using `rethindb dump`, [see the docs for more details](https://rethinkdb.com/docs/backup/). Copy the JSON files of the dump into the `db` directory you just created.

### 3. Copy the uploaded files from Yanius

The files are located in `<yanius root>/upload`. Copy them to the `files` directory.

### 4. Create a user map

Create the `usermap.yml` file. It must contain a mapping of old Yanius user UUIDs to OpenID `sub` IDs from your IDP. Files of missing users will not be imported!

```yaml
<yanius UUID>: <OpenID sub>
<yanius UUID>: <OpenID sub>
[...]
```

### 5. Start the import

Run the following command. The config should be the same as for the server. Adjust the path to point to the directory created in step 1.

```shell
yafus -c config.yml manage import yanius /some/path
```
