# Configuration

Yafus can be configured using a configuration file and optionally using environment variables.

[[_TOC_]]

## Configuration methods

### File

Only YAML is supported.

By default, Yafus tries to find a configuration file at `/etc/yafus/config.yml` or `/etc/yafus/config.yaml`, reading only the first one found. To change the path, specify `-c [file path]` (or `--config`).

### Environment variables

While the configuration file is preferred, all options are also read from environment variables, overriding options set in the file.

All options are in the format `YAFUS_<option>`, where `<option>` is the uppercased path of the option, separated with `_`.


## Structure

To get an up-to-date list of all config options with their default values, run [the command `yafus config generate`](cli/yafus_config_generate.md). 

Currently, the full configuration looks like this:

```yaml
auth:
  oidc:
    provider: ""
    clientID: ""
    scopePrefix: ""
database:
  connectionString: ""
general:
  logLevel: info
  logFormat: pretty
server:
  port: 8080
  trustedProxyCount: 0
storage:
  provider: local
  local:
    path: /var/lib/yafus/files
  s3:
    endpoint: ""
    useTLS: true
    SkipTLSVerification: false
    region: ""
    bucket: ""
    path: /
    accessKeyID: ""
    secretAccessKey: ""
```

While most options are self-explanatory, here are a few notes:

* `database`
  * `connectionString`: Is a full PostgreSQL connection string, like `host=db.example.com user=yafus-user password=yafus-password dbname=yafus port=5432 sslmode=require`
* `server`
  * `trustedProxyCount`: Set the count of proxies that are trusted for remote host determination, usually 1, defaults to 0. Uses the `X-Forwarded-For` header.
* `storage`
  * `provider`: Selects which provider is used, only the options for the selected one are used.
