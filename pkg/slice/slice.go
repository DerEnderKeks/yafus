package slice

func Filter[K comparable](s []K, condition func(K) bool) []K {
	var filtered []K
	for _, k := range s {
		if condition(k) {
			filtered = append(filtered, k)
		}
	}
	return filtered
}

func Find[K comparable](s []K, condition func(K) bool) *K {
	for _, k := range s {
		if condition(k) {
			return &k
		}
	}
	return nil
}

func Contains[K comparable](s []K, condition func(K) bool) bool {
	for _, k := range s {
		if condition(k) {
			return true
		}
	}
	return false
}
