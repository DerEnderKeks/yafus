package slice

import (
	"reflect"
	"testing"
)

func TestFilter(t *testing.T) {
	type args[K comparable] struct {
		s         []K
		condition func(K) bool
	}
	type testCase[K comparable] struct {
		name string
		args args[K]
		want []K
	}
	tests := []testCase[int]{
		{name: "even", args: args[int]{s: []int{1, 2, 3, 4, 5, 6, 7, 8, 9},
			condition: func(i int) bool { return i%2 == 0 }}, want: []int{2, 4, 6, 8}},
		{name: "odd", args: args[int]{s: []int{1, 2, 3, 4, 5, 6, 7, 8, 9},
			condition: func(i int) bool { return i%2 != 0 }}, want: []int{1, 3, 5, 7, 9}},
		{name: "greater7", args: args[int]{s: []int{1, 2, 3, 4, 5, 6, 7, 8, 9},
			condition: func(i int) bool { return i > 7 }}, want: []int{8, 9}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Filter(tt.args.s, tt.args.condition); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Filter() = %v, want %v", got, tt.want)
			}
		})
	}
}
