FROM node:20-alpine as frontend-builder

WORKDIR /usr/src/app
COPY internal/server/frontend .

RUN npm i
RUN npm run build

FROM golang:1.21-alpine as backend-builder

RUN apk update && apk add upx

WORKDIR /usr/src/app
COPY go.mod go.sum ./
RUN go mod download && go mod verify

COPY . .
COPY --from=frontend-builder /usr/src/app/dist internal/server/frontend/dist

RUN CGO_ENABLED=0 go build -ldflags="-w -s" -o /app ./cmd/yafus && \
    upx /app

FROM alpine:3.19

LABEL maintainer="DerEnderKeks"

COPY --from=backend-builder /app /app
USER 1000:1000

EXPOSE 8080

ENTRYPOINT ["/app", "-c", "/etc/yafus/config.yml", "serve"]
