package model

import (
	"github.com/google/uuid"
	"github.com/lib/pq"
	"gitlab.com/DerEnderKeks/yafus/internal/types"
	"time"
)

type File struct {
	ID        uuid.UUID  `json:"id" gorm:"default:(-)"`
	CreatedAt time.Time  `json:"createdAt"`
	UpdatedAt time.Time  `json:"updatedAt"`
	UserID    uuid.UUID  `json:"userID"`
	User      *User      `json:"user"`
	PublicID  string     `json:"publicID"`
	Name      *string    `json:"name,omitempty"`
	Extension *string    `json:"extension,omitempty"`
	Size      int64      `json:"size"`
	MimeType  string     `json:"mimeType"`
	Hash      types.Hash `json:"hash"`
}

type User struct {
	ID                uuid.UUID `json:"id" gorm:"default:(-)"`
	CreatedAt         time.Time `json:"createdAt"`
	UpdatedAt         time.Time `json:"updatedAt"`
	PreferredUsername *string   `json:"preferredUsername"`
	OpenidSub         string    `json:"openidSub"`
	UsedStorage       int64     `json:"usedStorage"`
}

type Token struct {
	ID        uuid.UUID      `json:"id" gorm:"default:(-)"`
	CreatedAt time.Time      `json:"createdAt"`
	UpdatedAt time.Time      `json:"updatedAt"`
	UserID    uuid.UUID      `json:"userID"`
	User      *User          `json:"user"`
	Hash      types.Hash     `json:"hash"`
	Scopes    pq.StringArray `json:"scopes" gorm:"type:text[]"`
}

func (Token) IsTokenBase()              {}
func (t Token) GetID() uuid.UUID        { return t.ID }
func (t Token) GetCreatedAt() time.Time { return t.CreatedAt }
func (t Token) GetUpdatedAt() time.Time { return t.UpdatedAt }
func (t Token) GetUserID() uuid.UUID    { return t.UserID }
func (t Token) GetUser() *User          { return t.User }
func (t Token) GetScopes() []string {
	if t.Scopes == nil {
		return nil
	}
	interfaceSlice := make([]string, 0, len(t.Scopes))
	for _, concrete := range t.Scopes {
		interfaceSlice = append(interfaceSlice, concrete)
	}
	return interfaceSlice
}

type TokenInput struct {
	Scopes []string `json:"scopes"`
}
