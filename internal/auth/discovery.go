package auth

import (
	"encoding/json"
	"fmt"
	"github.com/rs/zerolog/log"
	"net/http"
	"net/url"
)

type OIDCDiscovery struct {
	JWKSURI               string `json:"jwks_uri"`
	UserinfoEndpoint      string `json:"userinfo_endpoint"`
	AuthorizationEndpoint string `json:"authorization_endpoint"`
	TokenEndpoint         string `json:"token_endpoint"`
}

type OIDCConfig struct {
	Provider    string `json:"provider"`
	ClientID    string `json:"client_id"`
	ScopePrefix string `json:"scope_prefix"`
}

func (c *OIDCConfig) Discover() (*OIDCDiscovery, error) {
	d := &OIDCDiscovery{}

	discoveryEndpoint, err := url.JoinPath(c.Provider, ".well-known/openid-configuration")
	if err != nil {
		return nil, err
	}

	log.Info().Str("url", discoveryEndpoint).Msg("Discovering OIDC provider configuration")
	res, err := http.Get(discoveryEndpoint)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("failed to GET OIDC discovery endpoint: status %d", res.StatusCode)
	}

	if err = json.NewDecoder(res.Body).Decode(d); err != nil {
		return nil, err
	}

	log.Info().
		Str("userinfo", d.UserinfoEndpoint).
		Str("token", d.TokenEndpoint).
		Str("authorization", d.AuthorizationEndpoint).
		Str("jwks", d.JWKSURI).
		Msg("OIDC endpoints discovered")

	return d, nil
}
