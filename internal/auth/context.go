package auth

import (
	"context"
	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/DerEnderKeks/yafus/internal/model"
	"strings"
)

var AuthCtxKey = &contextKey{"auth"}

type contextKey struct {
	name string
}

// RequestAuth contains information about the authenticated user
// It always contains validated data, but fields may be nil
type RequestAuth struct {
	User  *model.User
	Token *jwt.Token

	// Scopes is a filtered list of scopes with the configured prefix removed
	Scopes *Scopes
}

// TokenClaims returns the Claims of the Token as TokenClaims or nil, when no Token is set
// This will intentionally panic if the Claims are the wrong type
func (ra *RequestAuth) TokenClaims() *TokenClaims {
	if ra.Token == nil {
		return nil
	}
	c := TokenClaims(ra.Token.Claims.(jwt.MapClaims))
	return &c
}

type TokenClaims jwt.MapClaims

func (c TokenClaims) Valid() error {
	return jwt.MapClaims(c).Valid()
}

// Scopes returns all scopes from the scope claim
func (c TokenClaims) Scopes() Scopes {
	scopeClaim, _ := c["scope"].(string)
	split := strings.Split(scopeClaim, " ")
	scopes := make(Scopes, len(split))
	for i, s := range split {
		scopes[i] = Scope(s)
	}
	return scopes
}

// Sub returns the sub claim or nil if missing
func (c TokenClaims) Sub() *string {
	subClaim, ok := c["sub"].(string)
	if !ok {
		return nil
	}
	return &subClaim
}

// ForContext gets the auth data from the context. Middleware.Middleware must have run to set the data. Can be nil.
func ForContext(ctx context.Context) *RequestAuth {
	raw, _ := ctx.Value(AuthCtxKey).(*RequestAuth)
	return raw
}
