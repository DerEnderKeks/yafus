package middleware

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/MicahParks/keyfunc"
	"github.com/golang-jwt/jwt/v4"
	"github.com/rs/zerolog/log"
	"gitlab.com/DerEnderKeks/yafus/internal/auth"
	"gitlab.com/DerEnderKeks/yafus/internal/model"
	"gitlab.com/DerEnderKeks/yafus/internal/server/responder"
	"gitlab.com/DerEnderKeks/yafus/internal/store"
	"gitlab.com/DerEnderKeks/yafus/internal/yerror"
	"io"
	"net/http"
	"strings"
	"time"
	"unsafe"
)

type Middleware struct {
	config    auth.OIDCConfig
	discovery auth.OIDCDiscovery
	store     *store.Store

	jwks *keyfunc.JWKS
}

func NewMiddleware(config auth.OIDCConfig, discovery auth.OIDCDiscovery, s *store.Store) (*Middleware, error) {
	m := &Middleware{
		config:    config,
		store:     s,
		discovery: discovery,
	}

	var err error
	m.jwks, err = keyfunc.Get(m.discovery.JWKSURI, keyfunc.Options{
		RefreshUnknownKID: true,
		RefreshInterval:   1 * time.Hour,
		RefreshTimeout:    10 * time.Second,
		RefreshRateLimit:  1 * time.Minute,
		RefreshErrorHandler: func(err error) {
			log.Error().Err(err).Msg("JWKS refresh failed")
		},
	})
	if err != nil {
		return nil, err
	}

	log.Info().Int("keyCount", m.jwks.Len()).Msg("JWKS fetched")
	return m, nil
}

func (m *Middleware) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authHeader := r.Header.Get("Authorization")

		// Auth header is missing, not trying to set user in context
		if authHeader == "" {
			next.ServeHTTP(w, r)
			return
		}

		plainAuthToken := strings.TrimPrefix(authHeader, "Bearer ")

		ra := &auth.RequestAuth{
			User:  &model.User{},
			Token: nil,
		}
		// JWT or static token?
		// JWTs are checked against the OIDC provider
		// Static tokens are checked internally
		if strings.IndexRune(plainAuthToken, '.') != -1 {
			token, err := jwt.ParseWithClaims(plainAuthToken, jwt.MapClaims{}, m.jwks.Keyfunc)
			if err != nil {
				responder.JSONError(w, http.StatusUnauthorized, err.Error())
				return
			}

			if !token.Valid {
				responder.JSONError(w, http.StatusUnauthorized, "Token signature invalid")
				return
			}

			err = token.Claims.Valid() // TODO add some tolerance for clock drift
			if err != nil {
				responder.JSONError(w, http.StatusUnauthorized, "Token expired")
				return
			}

			ra.Token = token
			sub := ra.TokenClaims().Sub()
			if sub == nil {
				responder.JSONError(w, http.StatusUnauthorized, "Token is missing sub claim")
				return
			}

			filteredScopes := auth.Scopes{}
			for _, scope := range ra.TokenClaims().Scopes() {
				if s, cut := strings.CutPrefix(string(scope), m.config.ScopePrefix); cut {
					filteredScopes = append(filteredScopes, auth.Scope(s))
				}
			}
			ra.Scopes = &filteredScopes

			ra.User, err = m.store.GetOrCreateUserBySub(r.Context(), *sub, fetchUsernameFunc(r.Context(), m.discovery.UserinfoEndpoint, authHeader))
			if err != nil {
				responder.JSONErrorInternal(w, err)
				return
			}
		} else {
			tokenWithUser, err := m.store.GetTokenWithUserByToken(r.Context(), plainAuthToken)
			if errors.Is(err, yerror.ErrNotFound) {
				responder.JSONError(w, http.StatusUnauthorized, "Invalid token")
				return
			} else if err != nil {
				responder.JSONErrorInternal(w, err)
				return
			}

			ra.User = tokenWithUser.User
			ra.Scopes = (*(auth.Scopes))(unsafe.Pointer(&tokenWithUser.Scopes)) // convert slice to different alias type
		}

		ctx := context.WithValue(r.Context(), auth.AuthCtxKey, ra)

		r = r.WithContext(ctx)
		next.ServeHTTP(w, r)
	})
}

func fetchUsernameFunc(ctx context.Context, url string, authHeader string) func() (*string, error) {
	return func() (*string, error) {
		uiReq, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
		if err != nil {
			return nil, err
		}

		uiReq.Header.Set("Authorization", authHeader)
		uiRes, err := http.DefaultClient.Do(uiReq)
		if err != nil {
			return nil, err
		}
		defer uiRes.Body.Close()

		if uiRes.StatusCode != http.StatusOK {
			body, _ := io.ReadAll(uiRes.Body)
			return nil, fmt.Errorf("failed to GET OIDC userinfo endpoint: status %d: %s", uiRes.StatusCode, body)
		}

		userinfo := struct {
			PreferredUsername string `json:"preferred_username"`
		}{}

		if err = json.NewDecoder(uiRes.Body).Decode(&userinfo); err != nil {
			return nil, err
		}

		return &userinfo.PreferredUsername, nil
	}
}
