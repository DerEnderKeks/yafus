package auth

import (
	"context"
	"github.com/google/uuid"
	"gitlab.com/DerEnderKeks/yafus/internal/yerror"
)

type Scope string
type Scopes []Scope

func (s Scopes) HasAny(scopes Scopes) bool {
	for _, v := range s {
		for _, scope := range scopes {
			if v == scope {
				return true
			}
		}
	}
	return false
}

// TODO rework, split up
const (
	ScopeAdminRead   = Scope("admin.read")
	ScopeAdminWrite  = Scope("admin.write")
	ScopeAdminDelete = Scope("admin.delete")
	ScopeSelfRead    = Scope("self.read")
	ScopeSelfWrite   = Scope("self.write")
	ScopeSelfDelete  = Scope("self.delete")
)

// ContextHasScope checks if a user is authenticated and has the scope
// By guarding a request with this, accessing the RequestAuth.User is always safe
func ContextHasScope(ctx context.Context, scope Scope) error {
	return contextIsUserIDAndHasScope(ctx, nil, scope)
}

// ContextIsUserIDAndHasScope checks if a user is authenticated, has the id and the scope
// By guarding a request with this, accessing the RequestAuth.User is always safe
func ContextIsUserIDAndHasScope(ctx context.Context, id uuid.UUID, scope Scope) error {
	return contextIsUserIDAndHasScope(ctx, &id, scope)
}

// ContextHasScopeOrIsUserIDAndHasScope checks if a user is authenticated and has the scope OR has the id and the userScope
// By guarding a request with this, accessing the RequestAuth.User is always safe
func ContextHasScopeOrIsUserIDAndHasScope(ctx context.Context, scope Scope, id *uuid.UUID, userScope Scope) error {
	return contextHasScopeOrIsUserIDAndHasScope(ctx, scope, id, userScope)
}

func contextIsUserIDAndHasScope(ctx context.Context, id *uuid.UUID, scope Scope) error {
	ra := ForContext(ctx)
	if ra == nil || ra.User == nil {
		return yerror.ErrUnauthorised
	}

	if (id == nil || ra.User.ID == *id) && ra.Scopes.HasAny(Scopes{scope}) {
		return nil
	}

	return yerror.ErrForbidden
}

func contextHasScopeOrIsUserIDAndHasScope(ctx context.Context, scope Scope, id *uuid.UUID, userScope Scope) error {
	ra := ForContext(ctx)
	if ra == nil || ra.User == nil {
		return yerror.ErrUnauthorised
	}

	if ((id != nil && ra.User.ID == *id) && ra.Scopes.HasAny(Scopes{userScope})) || ra.Scopes.HasAny(Scopes{scope}) {
		return nil
	}

	return yerror.ErrForbidden
}
