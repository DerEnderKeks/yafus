package yerror

import (
	"net/http"
)

type Error struct {
	code       int
	details    string
	msg        string
	extensions map[string]any
}

func (e Error) Error() string {
	if e.msg != "" {
		return e.msg
	}
	return e.CodeText()
}

func (e Error) Is(target error) bool {
	t, ok := target.(Error)
	if !ok {
		return false
	}
	return t.code == e.code
}

func (e Error) Code() int {
	return e.code
}

func (e Error) CodeText() string {
	switch e.code {
	case http.StatusForbidden:
		return "FORBIDDEN"
	case http.StatusInternalServerError:
		return "INTERNAL_SERVER_ERROR"
	case http.StatusNotFound:
		return "NOT_FOUND"
	case http.StatusUnauthorized:
		return "UNAUTHORIZED"
	default:
		return "UNKNOWN_ERROR"
	}
}

func (e Error) WithMsg(msg string) Error {
	e.msg = msg
	return e
}

func (e Error) WithDetails(details string) Error {
	e.details = details
	return e
}

type Extension struct {
	Key   string
	Value any
}

func (e Error) AddExtensions(extensions ...Extension) Error {
	if e.extensions == nil {
		e.extensions = map[string]any{}
	}
	for _, extension := range extensions {
		e.extensions[extension.Key] = extension.Value
	}
	return e
}

func (e Error) Extensions() map[string]any {
	_ = e.AddExtensions(
		Extension{Key: "details", Value: e.details},
		Extension{Key: "code", Value: e.CodeText()},
	)
	return e.extensions
}

var (
	ErrForbidden           = Error{code: http.StatusForbidden}
	ErrInternalServerError = Error{code: http.StatusInternalServerError}
	ErrNotFound            = Error{code: http.StatusNotFound}
	ErrUnauthorised        = Error{code: http.StatusUnauthorized}
)
