package config

import (
	"fmt"
	"github.com/cristalhq/aconfig"
	"github.com/go-playground/validator/v10"
	"github.com/rs/zerolog/log"
	"strings"
)

var validate *validator.Validate

func init() {
	validate = validator.New(validator.WithRequiredStructEnabled())

	customValidations := map[string]validator.Func{
		"storage_provider": ValidateStorageConfigProvider,
	}

	for tag, f := range customValidations {
		err := validate.RegisterValidation(tag, f)
		if err != nil {
			log.Fatal().Err(err).Str("tag", tag).Msg("Failed to register custom config validation function")
		}
	}
}

func (r Root) Validate(filters ...string) error {
	var err error
	if len(filters) == 0 {
		err = validate.Struct(r)
	} else {
		err = validate.StructFiltered(r, func(ns []byte) bool {
			for _, filter := range filters {
				if strings.HasPrefix(string(ns)+".", filter+".") {
					return false
				}
			}
			return true
		})
	}

	if err != nil {
		return fmt.Errorf("failed to validate config: %w", err)
	}
	return nil
}

type Section interface {
	StructPath() string
}

type Root struct {
	Auth     AuthConfig     `json:"auth" yaml:"auth"`
	Database DatabaseConfig `json:"database" yaml:"database"`
	General  GeneralConfig  `json:"general" yaml:"general"`
	Server   ServerConfig   `json:"server" yaml:"server"`
	Storage  StorageConfig  `json:"storage" yaml:"storage"`
}

func (r Root) StructPath() string {
	return "Root"
}

type AuthConfig struct {
	OIDC OIDCConfig `json:"oidc" yaml:"oidc"`
}

func (a AuthConfig) StructPath() string {
	return "Root.Auth"
}

type OIDCConfig struct {
	Provider    string `validate:"required,url" json:"provider" yaml:"provider"`
	ClientID    string `validate:"required" json:"clientID" yaml:"clientID"`
	ScopePrefix string `json:"scopePrefix" yaml:"scopePrefix"`
}

func (O OIDCConfig) StructPath() string {
	return "Root.Auth.OIDC"
}

type DatabaseConfig struct {
	ConnectionString string `validate:"required" json:"connectionString" yaml:"connectionString"`
}

func (d DatabaseConfig) StructPath() string {
	return "Root.Database"
}

type GeneralConfig struct {
	LogLevel  string `default:"info" usage:"disabled, panic, fatal, error, warn, info, debug, trace" validate:"omitempty,oneof=disabled panic fatal error warn info debug trace" json:"logLevel" yaml:"logLevel"`
	LogFormat string `default:"pretty" usage:"pretty, json" validate:"omitempty,oneof=pretty json" json:"logFormat" yaml:"logFormat"`
}

func (g GeneralConfig) StructPath() string {
	return "Root.General"
}

type ServerConfig struct {
	Port              uint `default:"8080" validate:"omitempty,gt=0,lte=65535" json:"port" yaml:"port"`
	TrustedProxyCount uint `default:"0" json:"trustedProxyCount" yaml:"trustedProxyCount"`
}

func (s ServerConfig) StructPath() string {
	return "Root.Server"
}

type StorageConfig struct {
	Provider string             `default:"local" usage:"local, s3" validate:"required,oneof=local s3,storage_provider" json:"provider" yaml:"provider"`
	Local    StorageLocalConfig `json:"local" yaml:"local"`
	S3       StorageS3Config    `json:"s3" yaml:"s3"`
}

func ValidateStorageConfigProvider(fl validator.FieldLevel) bool {
	var requiredValue string
	sc := fl.Parent().Interface().(StorageConfig)
	switch fl.Field().String() {
	case "local":
		requiredValue = sc.Local.Path
	case "s3":
		requiredValue = sc.S3.Endpoint
	}

	return len(requiredValue) > 0
}

func (s StorageConfig) StructPath() string {
	return "Root.Storage"
}

type StorageLocalConfig struct {
	Path string `default:"/var/lib/yafus/files" json:"path" yaml:"path"` // validated with ValidateStorageConfigProvider
}

func (s StorageLocalConfig) StructPath() string {
	return "Root.Storage.Local"
}

type StorageS3Config struct {
	Endpoint            string `json:"endpoint" yaml:"endpoint"` // validated with ValidateStorageConfigProvider
	UseTLS              bool   `default:"true" json:"useTLS" yaml:"useTLS"`
	SkipTLSVerification bool   `default:"false" json:"SkipTLSVerification" yaml:"SkipTLSVerification"`
	Region              string `json:"region" yaml:"region"`
	Bucket              string `validate:"required_with=Endpoint" json:"bucket" yaml:"bucket"`
	Path                string `default:"/" validate:"required_with=Endpoint" json:"path" yaml:"path"`
	AccessKeyID         string `validate:"required_with=Endpoint" json:"accessKeyID" yaml:"accessKeyID"`
	SecretAccessKey     string `validate:"required_with=Endpoint" json:"secretAccessKey" yaml:"secretAccessKey"`
}

func (s StorageS3Config) StructPath() string {
	return "Root.Storage.S3"
}

func GenerateDefault() (Root, error) {
	var defaultConfig Root
	loader := aconfig.LoaderFor(&defaultConfig, aconfig.Config{
		SkipDefaults:     false,
		SkipFiles:        true,
		SkipEnv:          true,
		SkipFlags:        true,
		DontGenerateTags: false,
	})

	err := loader.Load()
	if err != nil {
		return Root{}, err
	}

	return defaultConfig, nil
}
