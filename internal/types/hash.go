package types

import (
	"database/sql/driver"
	"encoding/hex"
	"fmt"
	"io"
	"strconv"
)

type Hash []byte

func (h Hash) Value() (driver.Value, error) {
	return []byte(h), nil
}

func (h Hash) MarshalGQL(w io.Writer) {
	w.Write([]byte(strconv.Quote(hex.EncodeToString(h))))
}

func (h *Hash) UnmarshalGQL(v interface{}) error {
	switch v := v.(type) {
	case string:
		b, err := hex.DecodeString(v)
		if err != nil {
			return err
		}
		*h = b
	default:
		return fmt.Errorf("%T is not a string", v)
	}
	return nil
}
