package server

import (
	"embed"
	"fmt"
	"html/template"
	"io/fs"
	"path"
	"strings"
)

func init() {
	if err := loadTemplates(); err != nil {
		panic(fmt.Errorf("failed to load templates: %v", err))
	}
}

//go:embed templates
var templateFS embed.FS
var templates map[string]*template.Template

func loadTemplates() error {
	templates = map[string]*template.Template{}
	templateDir := "templates"

	files, err := fs.ReadDir(templateFS, templateDir)
	if err != nil {
		return err
	}

	for _, file := range files {
		if file.IsDir() {
			continue
		}

		parsed, err := template.ParseFS(templateFS, path.Join(templateDir, file.Name()))
		if err != nil {
			return fmt.Errorf("failed to parse template: %s: %w", file.Name(), err)
		}

		cleanName := strings.TrimSuffix(file.Name(), ".tmpl")
		templates[cleanName] = parsed
	}

	return nil
}
