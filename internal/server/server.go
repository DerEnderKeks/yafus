package server

import (
	"context"
	"errors"
	"fmt"
	"github.com/99designs/gqlgen/graphql"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
	"github.com/vektah/gqlparser/v2/gqlerror"
	"gitlab.com/DerEnderKeks/yafus/internal/auth"
	authmw "gitlab.com/DerEnderKeks/yafus/internal/auth/middleware"
	"gitlab.com/DerEnderKeks/yafus/internal/server/graph"
	"gitlab.com/DerEnderKeks/yafus/internal/server/responder"
	"gitlab.com/DerEnderKeks/yafus/internal/store"
	"gitlab.com/DerEnderKeks/yafus/internal/yerror"
	"net/http"
	"time"
)

type Server struct {
	router        *mux.Router
	store         *store.Store
	port          uint
	oidcDiscovery *auth.OIDCDiscovery
	oidcConfig    auth.OIDCConfig
}

func NewServer(port uint, trustProxyCount uint, st *store.Store, oidcConfig auth.OIDCConfig) (*Server, error) {
	r := mux.NewRouter()
	s := &Server{
		router:     r,
		store:      st,
		port:       port,
		oidcConfig: oidcConfig,
	}

	var err error
	s.oidcDiscovery, err = oidcConfig.Discover()
	if err != nil {
		return nil, err
	}

	// TODO complexity
	graphSrv := handler.NewDefaultServer(graph.NewExecutableSchema(graph.Config{Resolvers: &graph.Resolver{Store: st}}))
	graphSrv.SetErrorPresenter(func(ctx context.Context, err error) *gqlerror.Error {
		var gqlErr *gqlerror.Error

		// error from gqlgen itself (like validation)
		if errors.As(err, &gqlErr) {
			wErr := errors.Unwrap(gqlErr)
			// TODO PgError leaks through, probably others too
			if wErr == nil || errors.Unwrap(wErr) == nil {
				return graphql.DefaultErrorPresenter(ctx, err)
			}
		}

		var yErr yerror.Error
		if !errors.As(err, &yErr) {
			log.Error().Err(err).Msg("Internal error occurred during Graphql handling")
			yErr = yerror.ErrInternalServerError
		}

		gqlErr = &gqlerror.Error{
			Message:    yErr.Error(),
			Path:       graphql.GetPath(ctx),
			Extensions: yErr.Extensions(),
		}

		return gqlErr
	})
	graphSrv.SetRecoverFunc(func(ctx context.Context, err interface{}) error {
		var msg string
		e, ok := err.(error)
		if ok {
			msg = e.Error()
		} else {
			msg = fmt.Sprintf("%v", e)
		}

		log.Error().Str("error", msg).Msg("Panic during GraphQL handling recovered")
		return yerror.ErrInternalServerError
	})
	graphRouter := r.PathPrefix("/graph").Subrouter()
	graphRouter.Handle("/query", graphSrv)

	authMW, err := authmw.NewMiddleware(oidcConfig, *s.oidcDiscovery, st)
	if err != nil {
		return nil, err
	}
	graphRouter.Use(authMW.Middleware)

	//r.Path("/").HandlerFunc(s.HandleLanding).Methods(http.MethodGet, http.MethodHead)
	r.Path("/").HandlerFunc(responder.CachedRedirect("/dashboard/", 1*time.Hour)).Methods(http.MethodGet, http.MethodHead)
	r.Path("/health").HandlerFunc(s.HandleHealth).Methods(http.MethodGet, http.MethodHead)
	r.Path("/auth/oidc-config").HandlerFunc(s.HandleOIDCConfig).Methods(http.MethodGet, http.MethodHead)
	r.Path("/dashboard").HandlerFunc(responder.RedirectToDirectory)
	r.PathPrefix("/dashboard").Handler(http.StripPrefix("/dashboard", http.HandlerFunc(s.HandleSPA)))
	r.Path("/{id}").HandlerFunc(s.HandleFile).Methods(http.MethodGet, http.MethodHead)

	r.Use(loggingMiddleware(trustProxyCount))
	// TODO add middleware that adds Allow header for OPTION requests

	return s, nil
}

func (s *Server) Start() error {
	return http.ListenAndServe(fmt.Sprintf(":%d", s.port), s.router)
}
