package responder

import (
	"encoding/json"
	"fmt"
	"github.com/rs/zerolog/log"
	"net/http"
	"strings"
	"time"
)

func CachedRedirect(url string, duration time.Duration) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Cache-Control", fmt.Sprintf("max-age=%d", int(duration.Seconds())))
		http.Redirect(w, r, url, http.StatusTemporaryRedirect)
	}
}

func RedirectToDirectory(w http.ResponseWriter, r *http.Request) {
	if !strings.HasSuffix(r.URL.Path, "/") {
		CachedRedirect(r.URL.Path+"/", 8*time.Hour).ServeHTTP(w, r)
		return
	}

	PlainError(w, http.StatusInternalServerError)
	log.Error().Str("url", r.URL.Path).Msg("RedirectToDirectory handler called for URL already ending with /")
}

func PlainError(w http.ResponseWriter, status int) {
	http.Error(w, http.StatusText(status), status)
}

func JSONError(w http.ResponseWriter, status int, err string) {
	e := struct {
		Error string `json:"error"`
	}{
		Error: err,
	}

	JSONResponse(w, status, e)
}

func JSONErrorInternal(w http.ResponseWriter, err error) {
	JSONError(w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError))
	log.Error().Err(err).Msg("Error during request")
}

func JSONResponse(w http.ResponseWriter, status int, data any) {
	j, err := json.Marshal(data)
	if err != nil {
		msg := "Failed to marshal JSON response"
		log.Error().Err(err).Msg(msg)
		http.Error(w, msg, http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	_, err = w.Write(j)
	if err != nil {
		log.Error().Err(err).Msg("Failed to reply to http request")
	}
}
