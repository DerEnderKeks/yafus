export const handleCopyButton = (value: string, event: Event) => {
	const copyClass = 'pi-copy'
	const checkClass = 'pi-check'

	navigator.clipboard.writeText(value).then(() => {
		let elem : Element|null = event.target as Element
		if (elem.hasChildNodes()) {
			elem = elem.querySelector("span[data-pc-section='icon']")
		}
		elem?.classList.replace(copyClass, checkClass)
		setTimeout(() => {
			elem?.classList.replace(checkClass, copyClass)
		}, 500)
	}).catch(err => {
		console.error(err)
	})
}
