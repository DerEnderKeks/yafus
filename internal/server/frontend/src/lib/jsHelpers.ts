export const forceArray = (a: any) => {
	if (Array.isArray(a)) return a
	return [a]
}
