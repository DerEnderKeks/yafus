// TODO sync multiple tabs (Broadcast Channels?)

import {generateCodeVerifier, OAuth2Client, OAuth2Fetch, OAuth2Token} from '@badgateway/oauth2-client';
import {GraphQLRequest} from "@apollo/client/link/core";
import {DefaultContext, ServerError} from "@apollo/client/core";
import {ContextSetter} from "@apollo/client/link/context";
import {ErrorResponse} from "@apollo/client/link/error";
import {promiseToObservable} from "./promiseToObservable";

const getStoredTokens = (): OAuth2Token | null => {
	const token = localStorage.getItem('tokenStore');
	if (token) return JSON.parse(token);
	return null;
}

const storeTokens = (token: OAuth2Token) => {
	localStorage.setItem('tokenStore', JSON.stringify(token));
}

const clearTokens = () => {
	localStorage.removeItem('tokenStore')
	sessionStorage.removeItem('codeVerifier')
}

const redirectURL = `${location.origin}/dashboard/oauth`

export interface AuthSettings {
	provider: string
	tokenEndpoint: string
	authorizationEndpoint: string
	clientId: string
	scopePrefix: string
}

export const Scopes = {
	adminRead: 'admin.read',
	adminWrite: 'admin.write',
	adminDelete: 'admin.delete',
	selfRead: 'self.read',
	selfWrite: 'self.write',
	selfDelete: 'self.delete',
}

export class Auth {
	settings: AuthSettings
	client: OAuth2Client
	oauth2Fetch: OAuth2Fetch
	scopes: {[key: string]: string}

	constructor(settings: AuthSettings) {
		this.settings = settings

		this.scopes = {...Scopes}

		Object.entries(this.scopes).forEach(([k, v]) => {
			// @ts-ignore
			this.scopes[k] = settings.scopePrefix + v
		})

		this.client = new OAuth2Client({
			clientId: settings.clientId,
			authenticationMethod: 'client_secret_post',
			tokenEndpoint: settings.tokenEndpoint,
			authorizationEndpoint: settings.authorizationEndpoint,
		})

		const scopes = Object.values(this.scopes)
		scopes.push('openid', 'profile', 'offline_access')
		this.oauth2Fetch = new OAuth2Fetch({
			async getNewToken(): Promise<OAuth2Token | null> {
				const codeVerifier = await generateCodeVerifier()
				sessionStorage.setItem('codeVerifier', codeVerifier)
				sessionStorage.setItem('preAuthLocation', location.href)
				document.location = await this.client.authorizationCode.getAuthorizeUri({
					redirectUri: redirectURL,
					codeVerifier,
					scope: scopes
				})
				return null
			},
			storeToken: storeTokens,
			getStoredToken: getStoredTokens,
			client: this.client,
		});
	}

	handleRedirect = async () => {
		const oauth2Token = await this.client.authorizationCode.getTokenFromCodeRedirect(
			document.location.href,
			{
				redirectUri: redirectURL,
				codeVerifier: sessionStorage.getItem('codeVerifier') || undefined,
			},
		)
		storeTokens(oauth2Token)
		const preAuthLocation = sessionStorage.getItem('preAuthLocation')
		sessionStorage.removeItem('preAuthLocation')
		sessionStorage.removeItem('codeVerifier')
		if (preAuthLocation) document.location = preAuthLocation
		else document.location = '/dashboard/'
	}

	authApolloMiddleware: ContextSetter = async (operation: GraphQLRequest, prevContext: DefaultContext): Promise<DefaultContext> => {
		const accessToken = await this.oauth2Fetch.getAccessToken()

		prevContext.headers = prevContext.headers || {}
		prevContext.headers.authorization = `Bearer ${accessToken}`
		return prevContext
	}

	authApolloErrorMiddleware = ({forward, operation, graphQLErrors, networkError, response}: ErrorResponse) => {
		if ((networkError as ServerError)?.statusCode === 401) {
			console.log("Error 401, refreshing token and retrying operation...")
			const newTokenPromise = this.oauth2Fetch.refreshToken();
			return promiseToObservable(newTokenPromise).flatMap((newToken) => {
				const ctx = operation.getContext()
				ctx.headers = ctx?.headers || {}
				ctx.headers.authorization = `Bearer ${(newToken as OAuth2Token).accessToken}`
				operation.setContext(ctx)

				return forward(operation)
			})
		}
	}

	logout = () => {
		clearTokens()
		document.location = '/dashboard/'
	}
}

export {}
