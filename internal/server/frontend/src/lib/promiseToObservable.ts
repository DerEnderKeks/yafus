import {Observable} from "@apollo/client/utilities";

export const promiseToObservable = (promise: any) => {
	return new Observable((subscriber) => {
		promise.then(
			(value: any) => {
				if (subscriber.closed) {
					return;
				}

				subscriber.next(value);
				subscriber.complete();
			},
			(err: any) => {
				subscriber.error(err);
			},
		);
	});
};
