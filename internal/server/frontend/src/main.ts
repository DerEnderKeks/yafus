import {createApp, h, provide} from 'vue'
import {createRouter, createWebHistory} from 'vue-router'

import {ApolloClient, concat, createHttpLink, InMemoryCache} from '@apollo/client/core'
import { loadErrorMessages, loadDevMessages } from '@apollo/client/dev'
import {setContext} from '@apollo/client/link/context'
import {DefaultApolloClient} from '@vue/apollo-composable'
// @ts-ignore
import createUploadLink from 'apollo-upload-client/createUploadLink.mjs'

import PrimeVue from 'primevue/config';

import 'primevue/resources/themes/viva-dark/theme.css'
import 'primevue/resources/primevue.min.css'
import 'primeicons/primeicons.css'
import 'primeflex/primeflex.css'

import Tooltip from 'primevue/tooltip'
import Ripple from 'primevue/ripple'
import StyleClass from 'primevue/styleclass'
import BadgeDirective from 'primevue/badgedirective'
import ConfirmationService from 'primevue/confirmationservice'
import ToastService from 'primevue/toastservice'

import './style.css'
import App from './App.vue'

import {Auth, AuthSettings} from './lib/auth'

import Files from './components/Files.vue'
import Users from './components/Users.vue'
import NotFound from './components/NotFound.vue'
import OAuth from './components/OAuth.vue'
import {onError} from '@apollo/client/link/error'
import Tokens from './components/Tokens.vue'

const authOptionsResponse = await fetch('/auth/oidc-config')
const authOptionsRaw = await authOptionsResponse.json()
const authOptions: AuthSettings = {
	scopePrefix: authOptionsRaw.scope_prefix,
	clientId: authOptionsRaw.client_id,
	authorizationEndpoint: authOptionsRaw.authorization_endpoint,
	tokenEndpoint: authOptionsRaw.token_endpoint,
	provider: authOptionsRaw.provider,
}

const auth = new Auth(authOptions)

// const apolloHttpLink = createHttpLink({
// 	uri: '/graph/query',
// })

const apolloHttpLink = createUploadLink({ uri: '/graph/query' })

const authMiddleware = setContext(auth.authApolloMiddleware);
const authErrorMiddleware = onError(auth.authApolloErrorMiddleware);

const apolloCache = new InMemoryCache()

const apolloClient = new ApolloClient({
	link: concat(concat(authMiddleware, authErrorMiddleware), apolloHttpLink),
	cache: apolloCache,
	connectToDevTools: true,
})

// @ts-ignore
if (process.env.NODE_ENV == 'development') {
	loadDevMessages()
	loadErrorMessages()
}

const routes = [
	{path: '/', redirect: {name: 'files'}},
	{path: '/files', name: 'files', component: Files},
	{path: '/tokens', name: 'tokens', component: Tokens},
	{path: '/users', name: 'users', component: Users},
	{path: '/users/:id/files', name: 'user-files', component: Files},
	{path: '/users/:id/tokens', name: 'user-tokens', component: Tokens},
	{path: '/oauth', name: 'oauth', component: OAuth},
	{path: '/:catchall(.*)*', name: 'not-found', component: NotFound},
]

const router = createRouter({
	history: createWebHistory('/dashboard'),
	routes: routes,
})

const app = createApp({
	setup() {
		provide(DefaultApolloClient, apolloClient)
	},
	render: () => h(App),
})

app.use(router)

app.use(PrimeVue, {inputStyle: 'filled'})
app.use(ConfirmationService)
app.use(ToastService)
app.directive('tooltip', Tooltip)
app.directive('ripple', Ripple)
app.directive('styleclass', StyleClass)
app.directive('badge', BadgeDirective)

app.provide('auth', auth)

app.mount('#app')
