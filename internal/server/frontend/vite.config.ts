/// <reference types="vite/client" />

import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'

// this plugin enables similar behavior for the vite dev server compared to when the frontend is embedded in the backend
const baseRewritePlugin = () => ({
	name: 'configure-server',
	configureServer(server:any) {
		server.middlewares.use((req:any, res:any, next:any) => {
			if (req.url.startsWith('/dashboard/') && !req.url.startsWith('/dashboard/static/')) {
				req.url = '/dashboard/static/'
			}
			next()
		})
	},
})

// https://vitejs.dev/config/
export default defineConfig({
	plugins: [vue(), baseRewritePlugin()],
	build: {
		target: ['es2022']
	},
	base: '/dashboard/static/',
	server: {
		proxy: {
			// proxy anything but the dashboard to the backend
			'^\/(?!dashboard\/)': {
				target: 'http://localhost:8080',
				changeOrigin: true,
			}
		}
	},
})
