package graph

import "gitlab.com/DerEnderKeks/yafus/internal/store"

//go:generate go run github.com/99designs/gqlgen generate

type Resolver struct {
	Store *store.Store
}
