package server

import (
	"crypto/sha256"
	"embed"
	"encoding/hex"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
	"gitlab.com/DerEnderKeks/yafus/internal/auth"
	"gitlab.com/DerEnderKeks/yafus/internal/server/responder"
	"gitlab.com/DerEnderKeks/yafus/internal/yerror"
	"io"
	"io/fs"
	"net/http"
	"os"
	"path"
	"strconv"
	"strings"
	"sync"
)

func init() {
	if _, err := frontendFS.ReadFile(path.Join(frontendSubPath, "index.html")); err != nil {
		panic("no index.html file in embedded frontend, this build is probably broken")
	}

	var err error
	frontendSubFS, err = fs.Sub(frontendFS, frontendSubPath)
	if err != nil {
		panic(err)
	}
}

//go:embed frontend/dist/*
var frontendFS embed.FS
var frontendSubFS fs.FS

const frontendSubPath = "frontend/dist"

type etagCache struct {
	mu    sync.RWMutex
	cache map[string]string
	fs    *fs.FS
}

func newEtagCache(fs *fs.FS) *etagCache {
	return &etagCache{cache: map[string]string{}, fs: fs}
}

func (c *etagCache) get(filePath string) string {
	if strings.HasSuffix(filePath, "/") {
		filePath += "index.html"
	}
	filePath = strings.TrimPrefix(filePath, "/")
	c.mu.RLock()
	etag, ok := c.cache[filePath]
	c.mu.RUnlock()
	if ok {
		return etag
	}

	f, err := (*c.fs).Open(filePath)
	if err != nil {
		log.Error().Err(err).Str("path", filePath).Msg("Failed to open file for etag cache")
		return ""
	}

	fi, err := f.Stat()
	if err != nil {
		log.Error().Err(err).Str("path", filePath).Msg("Failed to stat file for etag cache")
		return ""
	}
	if !fi.Mode().IsRegular() {
		return ""
	}

	h := sha256.New()
	n, _ := io.Copy(h, f)
	if n != fi.Size() {
		log.Error().Int64("expected", fi.Size()).Int64("actual", n).Msg("Etag sum calculation byte count mismatch")
		return ""
	}

	hash := h.Sum(nil)
	if len(hash) != 32 {
		log.Error().Int("expected", 32).Int("actual", len(hash)).Msg("Etag sum length invalid")
		return ""
	}

	log.Info().Str("path", filePath).Msg("Adding file to etag cache")

	c.mu.Lock()
	defer c.mu.Unlock()
	c.cache[filePath] = hex.EncodeToString(hash)
	return c.cache[filePath]
}

var frontendEtagCache = newEtagCache(&frontendSubFS)

// HandleSPA serves the embedded SPA directory
func (s *Server) HandleSPA(w http.ResponseWriter, r *http.Request) {
	if !strings.HasPrefix(r.URL.Path, "/static/") {
		r.URL.Path = "/static/" // force return the index file for SPA routes
	} else {
		_, err := frontendSubFS.Open(strings.TrimPrefix(r.URL.Path, "/static/"))
		if os.IsNotExist(err) {
			http.NotFound(w, r)
			return
		}
	}

	if strings.HasPrefix(r.URL.Path, "/static/assets/") {
		// files have unique names and can thus be cached as immutable
		w.Header().Set("Cache-Control", "max-age=2592000, immutable") // one month
	} else {
		// non-unique files get an etag
		etag := frontendEtagCache.get(path.Join("/", strings.TrimPrefix(r.URL.Path, "/static/")))
		if etag != "" {
			w.Header().Add("ETag", strconv.Quote(etag))
		}
	}
	http.StripPrefix("/static/", http.FileServer(http.FS(frontendSubFS))).ServeHTTP(w, r)
}

func (s *Server) HandleLanding(w http.ResponseWriter, r *http.Request) {
	s.RenderTemplate(w, "landing.html")
}

// HandleHealth returns the health status of the server
func (s *Server) HandleHealth(w http.ResponseWriter, r *http.Request) {
	dbHealthy := s.store.IsDBHealthy()
	storageHealthy := s.store.IsStorageHealthy()

	status := http.StatusOK
	if !(dbHealthy && storageHealthy) {
		status = http.StatusServiceUnavailable
	}

	responder.JSONResponse(w, status, struct {
		Database bool `json:"database"`
		Storage  bool `json:"storage"`
	}{
		Database: dbHealthy,
		Storage:  storageHealthy,
	})
}

func (s *Server) HandleOIDCConfig(w http.ResponseWriter, r *http.Request) {
	response := struct {
		auth.OIDCDiscovery
		auth.OIDCConfig
	}{
		OIDCDiscovery: *s.oidcDiscovery,
		OIDCConfig:    s.oidcConfig,
	}
	responder.JSONResponse(w, http.StatusOK, response)
}

func (s *Server) HandleFile(w http.ResponseWriter, r *http.Request) {
	id, _, _ := strings.Cut(mux.Vars(r)["id"], ".")

	file, reader, err := s.store.GetFileWithBlob(r.Context(), id)
	if errors.Is(err, yerror.ErrNotFound) {
		responder.PlainError(w, http.StatusNotFound)
		return
	} else if err != nil {
		log.Error().Err(err).Str("publicID", id).Msg("Failed to get file")
		responder.PlainError(w, http.StatusInternalServerError)
		return
	}

	// these flags allow access to most features while still preventing access to any data storage
	cspSandboxFlags := []string{
		"allow-scripts",
		"allow-modals",
		"allow-forms",
		"allow-popups",
		"allow-downloads",
		"allow-orientation-lock",
		"allow-pointer-lock",
	}
	csps := []string{
		"sandbox " + strings.Join(cspSandboxFlags, " "),
		"worker-src 'none'",
	}
	// by setting this header it is possible to display HTML pages inline, without risking XSS
	w.Header().Set("Content-Security-Policy", strings.Join(csps, "; ")+";")

	w.Header().Set("Content-Type", file.MimeType)
	w.Header().Set("Content-Disposition",
		fmt.Sprintf("inline; filename=\"%s\"", escapeDispositionFilename(*file.Name)),
	)

	w.Header().Set("ETag", strconv.Quote(hex.EncodeToString(file.Hash)))
	w.Header().Set("Cache-Control", "no-cache") // ensure that deleted files are not cached

	http.ServeContent(w, r, *file.Name, file.CreatedAt, reader)
}

var escapeDispositionFilenameReplacer = strings.NewReplacer(
	"\\", "\\\\",
	"\"", "\\\"",
)

func escapeDispositionFilename(s string) string {
	return escapeDispositionFilenameReplacer.Replace(s)
}
