package server

import (
	"github.com/felixge/httpsnoop"
	"github.com/rs/zerolog/log"
	"gitlab.com/DerEnderKeks/yafus/internal/auth"
	"math"
	"net"
	"net/http"
	"net/netip"
	"strings"
)

// getClientIP returns the client ip with the specified amount of proxies taken into account
func getClientIP(r *http.Request, trustProxyCount uint) (ip net.IP) {
	for trustProxyCount > 0 {
		fwdHeaders := r.Header.Values("X-Forwarded-For")
		var fwdList []string
		for _, fwdIPs := range fwdHeaders {
			for _, fwdIP := range strings.Split(fwdIPs, ",") {
				fwdIP = strings.TrimSpace(fwdIP)
				if fwdIP == "" {
					continue
				}
				fwdList = append(fwdList, fwdIP)
			}
		}

		// no proxy present, use remote addr
		if len(fwdList) == 0 {
			break
		}

		// get the last possible trusted ip
		ip = net.ParseIP(fwdList[int(math.Max(0, float64(len(fwdList)-int(trustProxyCount))))])
		return
	}

	addrPort, err := netip.ParseAddrPort(r.RemoteAddr)
	if err == nil { // shouldn't fail
		ip = addrPort.Addr().AsSlice()
	}
	return
}

func loggingMiddleware(trustProxyCount uint) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			m := httpsnoop.CaptureMetrics(next, w, r)

			ip := getClientIP(r, trustProxyCount)

			msg := log.Info().
				Str("path", r.URL.Path).
				IPAddr("source", ip).
				Str("method", r.Method).
				Int("status", m.Code).
				Dur("duration", m.Duration)

			ra := auth.ForContext(r.Context())
			if ra != nil && ra.User != nil {
				msg.Str("userID", ra.User.ID.String())
			}

			msg.Msg("Request handled")
		})
	}
}
