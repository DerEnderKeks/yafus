package server

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/DerEnderKeks/yafus/internal/server/responder"
	"net/http"
)

func (s *Server) RenderTemplate(w http.ResponseWriter, tName string) {
	t, ok := templates[tName]
	if !ok {
		log.Error().Str("template", tName).Msg("Template doesn't exist")
		responder.PlainError(w, http.StatusInternalServerError)
		return
	}

	if err := t.Execute(w, nil); err != nil {
		log.Error().Err(err).Msg("Failed to execute template")
		responder.PlainError(w, http.StatusInternalServerError)
	}
}
