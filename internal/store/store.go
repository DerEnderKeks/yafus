package store

import (
	"fmt"
	"github.com/rs/zerolog/log"
	"gitlab.com/DerEnderKeks/yafus/internal/config"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"math/rand"
	"strings"
)

type Store struct {
	db              *gorm.DB
	storageProvider StorageProvider
}

func NewStore(dbConfig config.DatabaseConfig, storageConfig config.StorageConfig) (*Store, error) {
	gormDB, err := gorm.Open(postgres.Open(dbConfig.ConnectionString), &gorm.Config{
		Logger: logger.Discard.LogMode(logger.Silent),
	})
	if err != nil {
		return nil, err
	}

	store := &Store{
		db: gormDB,
	}

	switch storageConfig.Provider {
	case "local":
		store.storageProvider, err = NewLocalStorageProvider(storageConfig.Local)
	case "s3":
		store.storageProvider, err = NewS3StorageProvider(storageConfig.S3)
	default:
		err = fmt.Errorf("storage provider not implemented: %s", storageConfig.Provider)
	}

	if err != nil {
		return nil, fmt.Errorf("failed to initialize storage provider: %s: %w", storageConfig.Provider, err)
	}

	err = store.MigrateUpTo(MigrateLatestVersion)
	if err != nil {
		return nil, fmt.Errorf("failed to migrate db: %w", err)
	}

	return store, nil
}

func (s *Store) IsDBHealthy() bool {
	sql, err := s.db.DB()
	if err != nil {
		log.Error().Err(err).Msg("Failed to access underlying database connection")
		return false
	}

	err = sql.Ping()
	if err != nil {
		log.Error().Err(err).Msg("Database ping failed")
		return false
	}

	_, err = sql.Exec("SELECT 1+1")
	if err != nil {
		log.Error().Err(err).Msg("Database test query failed")
		return false
	}

	return true
}

func (s *Store) IsStorageHealthy() bool {
	return s.storageProvider.IsHealthy()
}

const randomIDChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_"

func randomID(n int) string {
	if n < 0 {
		panic("n must be positive")
	}

	sb := strings.Builder{}
	sb.Grow(n)
	for i := 0; i < n; i++ {
		sb.WriteByte(randomIDChars[rand.Int63()%int64(len(randomIDChars))])
	}
	return sb.String()
}
