package store

import (
	"bytes"
	"context"
	"crypto/tls"
	"errors"
	"fmt"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"github.com/rs/zerolog/log"
	"gitlab.com/DerEnderKeks/yafus/internal/config"
	"io"
	"io/fs"
	"net/http"
	"os"
	"path"
	"time"
)

type StorageProvider interface {
	// CreateFile atomically creates the file
	// If the file already exists, the error will be nil
	CreateFile(ctx context.Context, hash string, rs io.ReadSeeker, size int64, contentType string) error
	// GetFile returns a file handle
	GetFile(ctx context.Context, hash string) (io.ReadSeeker, error)
	// DeleteFile deletes the file
	DeleteFile(ctx context.Context, hash string) error
	// IsHealthy checks if the storage is healthy
	IsHealthy() bool
}

type LocalStorageProvider struct {
	path string
}

func (l *LocalStorageProvider) CreateFile(ctx context.Context, hash string, rs io.ReadSeeker, size int64, contentType string) error {
	targetDirPath := path.Join(l.path, fileHashSubDirPath(hash))

	info, err := os.Stat(targetDirPath)
	if errors.Is(err, fs.ErrNotExist) {
		err = os.MkdirAll(targetDirPath, os.ModePerm)
		if err != nil {
			return err
		}
	} else if err != nil {
		return err
	} else if !info.IsDir() {
		return fmt.Errorf("target sub path exists but is not a directory: %s", targetDirPath)
	}

	targetFilePath := path.Join(l.path, fileHashSubPath(hash))

	info, err = os.Stat(targetFilePath)
	if errors.Is(err, fs.ErrNotExist) {
		f, err := os.CreateTemp(targetDirPath, fmt.Sprintf("_%s_*", hash))
		if err != nil {
			return err
		}
		copySucceeded := false
		defer func() {
			_ = f.Close()
			if !copySucceeded {
				log.Debug().Str("path", f.Name()).Msg("Upload failed, deleting temporary file")
				_ = os.Remove(f.Name())
			}
		}()

		n, err := io.Copy(f, rs)
		if err != nil {
			return err
		}

		err = f.Close()
		if err != nil {
			return err
		}

		if n != size {
			return fmt.Errorf("written byte count mismatch: %d != %d", n, size)
		}

		// ensures that the file is created atomically
		// failed uploads won't block the hash and the same file can be uploaded multiple times at once
		err = os.Rename(f.Name(), targetFilePath)
		if err != nil {
			return err
		}

		copySucceeded = true
	} else if err != nil {
		return err
	} else if !info.Mode().IsRegular() {
		return fmt.Errorf("target path exists but is not a file: %s", targetFilePath)
	}

	return nil
}

func (l *LocalStorageProvider) GetFile(ctx context.Context, hash string) (io.ReadSeeker, error) {
	return os.Open(path.Join(l.path, fileHashSubPath(hash)))
}

func (l *LocalStorageProvider) DeleteFile(ctx context.Context, hash string) error {
	return os.Remove(path.Join(l.path, fileHashSubPath(hash)))
}

func (l *LocalStorageProvider) IsHealthy() bool {
	stat, err := os.Stat(l.path)
	if err == nil && !stat.IsDir() {
		err = fmt.Errorf("path is not a directory")
	}

	if err != nil {
		log.Error().Err(err).Msg("Local storage health check failed")
		return false
	}

	return true
}

func NewLocalStorageProvider(localConfig config.StorageLocalConfig) (*LocalStorageProvider, error) {
	log.Info().Str("path", localConfig.Path).Msg("Initializing local storage provider")

	lsp := LocalStorageProvider{
		path: localConfig.Path,
	}

	info, err := os.Stat(lsp.path)
	if err != nil {
		return nil, fmt.Errorf("failed to stat file path: %v", err)
	}

	if !info.IsDir() {
		return nil, fmt.Errorf("file path is not a directory")
	}

	log.Info().Msg("Testing local storage write access")
	if err = testLocalWrite(lsp.path); err != nil {
		return nil, fmt.Errorf("write test failed: %v", err)
	}

	log.Info().Msg("Local storage write access test succeeded")
	return &lsp, nil
}

type S3StorageProvider struct {
	mc     *minio.Client
	bucket string
	path   string
}

func (s *S3StorageProvider) CreateFile(ctx context.Context, hash string, rs io.ReadSeeker, size int64, contentType string) error {
	targetName := path.Join(s.path, fileHashSubPath(hash))

	_, err := s.mc.StatObject(ctx, s.bucket, targetName, minio.StatObjectOptions{})
	var errResp minio.ErrorResponse
	if err != nil && !(errors.As(err, &errResp) && errResp.Code == "NoSuchKey") {
		return err
	} else if err == nil {
		return nil // already exists, nothing to do
	}

	_, err = s.mc.PutObject(ctx, s.bucket, targetName, rs, size, minio.PutObjectOptions{
		ContentType:        contentType,
		ContentDisposition: "inline",
		CacheControl:       "no-cache",
	})
	return err
}

func (s *S3StorageProvider) GetFile(ctx context.Context, hash string) (io.ReadSeeker, error) {
	targetName := path.Join(s.path, fileHashSubPath(hash))
	_, err := s.mc.StatObject(ctx, s.bucket, targetName, minio.StatObjectOptions{})
	if err != nil {
		return nil, err
	}
	return s.mc.GetObject(ctx, s.bucket, targetName, minio.GetObjectOptions{})
}

func (s *S3StorageProvider) DeleteFile(ctx context.Context, hash string) error {
	targetName := path.Join(s.path, fileHashSubPath(hash))
	return s.mc.RemoveObject(ctx, s.bucket, targetName, minio.RemoveObjectOptions{})
}

func (s *S3StorageProvider) IsHealthy() bool {
	err := s.checkBucketExistence()
	if err != nil {
		log.Error().Err(err).Msg("S3 health check failed")
		return false
	}
	return s.mc.IsOnline()
}

func (s *S3StorageProvider) checkBucketExistence() error {
	exists, err := s.mc.BucketExists(context.Background(), s.bucket)
	if err != nil {
		return fmt.Errorf("failed to check if bucket exists: %s: %w", s.bucket, err)
	}
	if !exists {
		return fmt.Errorf("bucket doesn't exist or the token has no access: %s", s.bucket)
	}
	return nil
}

func NewS3StorageProvider(s3Config config.StorageS3Config) (*S3StorageProvider, error) {
	log.Info().Str("endpoint", s3Config.Endpoint).
		Str("bucket", s3Config.Bucket).
		Str("region", s3Config.Region).
		Str("path", s3Config.Path).Msg("Initializing S3 storage provider")

	s3sp := S3StorageProvider{
		bucket: s3Config.Bucket,
		path:   s3Config.Path,
	}

	tr := http.DefaultTransport.(*http.Transport).Clone()
	tr.TLSClientConfig = &tls.Config{InsecureSkipVerify: s3Config.SkipTLSVerification}

	var err error
	s3sp.mc, err = minio.New(s3Config.Endpoint, &minio.Options{
		Creds:     credentials.NewStaticV4(s3Config.AccessKeyID, s3Config.SecretAccessKey, ""),
		Secure:    s3Config.UseTLS,
		Transport: tr,
		Region:    s3Config.Region,
	})

	if err != nil {
		return nil, fmt.Errorf("failed to initialize S3 client: %w", err)
	}

	log.Info().Msg("Testing S3 connectivity and bucket access")

	err = s3sp.checkBucketExistence()
	if err != nil {
		return nil, err
	}

	testFileName := ".test"
	_, err = s3sp.mc.PutObject(context.Background(), s3sp.bucket, testFileName, bytes.NewReader([]byte{}), 0, minio.PutObjectOptions{})
	if err != nil {
		return nil, fmt.Errorf("failed to create test file: %w", err)
	}

	err = s3sp.mc.RemoveObject(context.Background(), s3sp.bucket, testFileName, minio.RemoveObjectOptions{})
	if err != nil {
		return nil, fmt.Errorf("failed to delete test file: %w", err)
	}

	_, err = s3sp.mc.HealthCheck(10 * time.Second)

	if err != nil {
		return nil, fmt.Errorf("failed to start S3 client health check: %w", err)
	}

	log.Info().Msg("S3 connectivity test succeeded")

	return &s3sp, nil
}

// fileHashSubDirPath returns a path in the form of "ab/cd" given a hash of "abcdefgh..."
func fileHashSubDirPath(hash string) string {
	if len(hash) < 5 {
		log.Error().Str("hash", hash).Msg("Tried to build a subpath for a hash shorter than 5 characters. This is a bug.")
		return ""
	}
	return path.Join(hash[:2], hash[2:4])
}

// fileHashSubPath returns a path in the form of "ab/cd/abcdefgh..." given a hash of "abcdefgh..."
func fileHashSubPath(hash string) string {
	return path.Join(fileHashSubDirPath(hash), hash)
}

// testLocalWrite tests whether dir is writable
func testLocalWrite(dir string) error {
	testDirPath := path.Join(dir, ".testdir")

	_, err := os.Stat(testDirPath)
	if err != nil && !errors.Is(err, fs.ErrNotExist) {
		return err
	} else if err == nil {
		err = os.RemoveAll(testDirPath)
		if err != nil {
			return err
		}
	}

	err = os.Mkdir(testDirPath, os.ModePerm)
	if err != nil {
		return err
	}

	testFilePath := path.Join(testDirPath, ".testfile")
	f, err := os.Create(testFilePath)

	if err != nil {
		return err
	}

	_ = f.Close()

	err = os.RemoveAll(testDirPath)
	return err
}
