package store

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"github.com/99designs/gqlgen/graphql"
	"github.com/gabriel-vasile/mimetype"
	"github.com/google/uuid"
	"github.com/jackc/pgerrcode"
	"github.com/jackc/pgx/v5/pgconn"
	"gitlab.com/DerEnderKeks/yafus/internal/auth"
	"gitlab.com/DerEnderKeks/yafus/internal/model"
	"gitlab.com/DerEnderKeks/yafus/internal/yerror"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"io"
	"path/filepath"
	"strings"
	"time"
)

func (s *Store) GetFiles(ctx context.Context, userID *uuid.UUID) ([]*model.File, error) {
	if err := auth.ContextHasScopeOrIsUserIDAndHasScope(ctx, auth.ScopeAdminRead, userID, auth.ScopeSelfRead); err != nil {
		return nil, err
	}

	var files []*model.File
	var err error
	if userID == nil {
		err = s.db.Find(&files).Error
	} else {
		err = s.db.Where(&model.File{UserID: *userID}).Find(&files).Error
	}
	return files, err
}

func (s *Store) CreateFile(ctx context.Context, upload graphql.Upload) (*model.File, error) {
	if err := auth.ContextHasScope(ctx, auth.ScopeSelfWrite); err != nil {
		return nil, err
	}
	userID := auth.ForContext(ctx).User.ID

	return s.CreateFileWithTimeAndUserIDAndPublicID(ctx, upload, nil, userID, nil)
}

func (s *Store) CreateFileWithTimeAndUserIDAndPublicID(ctx context.Context, upload graphql.Upload, timestamp *time.Time, userID uuid.UUID, publicID *string) (*model.File, error) {
	mime, err := mimetype.DetectReader(upload.File)
	if err != nil {
		return nil, fmt.Errorf("mime type detection failed: %v", err)
	}

	seek, err := upload.File.Seek(0, io.SeekStart)
	if err != nil {
		return nil, err
	}
	if seek != 0 {
		return nil, fmt.Errorf("seek after mime type detection failed")
	}

	h := sha256.New()
	n, _ := io.Copy(h, upload.File)
	if n != upload.Size {
		return nil, fmt.Errorf("sum calculation byte count mismatch: %d != %d", n, upload.Size)
	}

	hash := h.Sum(nil)

	if len(hash) != 32 {
		return nil, fmt.Errorf("hash length invalid: %d != 32", len(hash))
	}

	seek, err = upload.File.Seek(0, io.SeekStart)
	if err != nil {
		return nil, err
	}
	if seek != 0 {
		return nil, fmt.Errorf("seek after hashing failed")
	}

	ext := strings.TrimLeft(filepath.Ext(upload.Filename), ".")
	extPtr := &ext
	if ext == "" {
		extPtr = nil
	}

	newFile := &model.File{
		UserID:    userID,
		Name:      &upload.Filename,
		Extension: extPtr,
		Size:      upload.Size,
		MimeType:  mime.String(),
		Hash:      hash,
	}

	if timestamp != nil {
		newFile.CreatedAt = *timestamp
		newFile.UpdatedAt = *timestamp
	}

	err = s.db.Transaction(func(tx *gorm.DB) error {
		err := tx.SavePoint("start").Error
		if err != nil {
			return err
		}

		retryLimit := 100
		for {
			if publicID != nil {
				newFile.PublicID = *publicID
				retryLimit = 0
			} else {
				newFile.PublicID = randomID(10)
			}

			err = tx.Create(newFile).Error
			var pgErr *pgconn.PgError
			if errors.As(err, &pgErr) &&
				pgErr.Code == pgerrcode.UniqueViolation &&
				pgErr.ConstraintName == "files_public_id_key" {
				if retryLimit > 0 {
					err := tx.RollbackTo("start").Error
					if err != nil {
						return err
					}
					retryLimit--
					continue
				}

				return fmt.Errorf("failed to assign unique public ID")
			} else if err != nil {
				return err
			}

			break
		}

		hashString := hex.EncodeToString(hash)
		err = s.storageProvider.CreateFile(ctx, hashString, upload.File, upload.Size, newFile.MimeType)
		if err != nil {
			return err
		}

		user := &model.User{
			ID: newFile.UserID,
		}
		err = tx.Clauses(clause.Locking{Strength: "UPDATE"}).First(user).Error
		if err != nil {
			return err
		}

		user.UsedStorage += upload.Size
		err = tx.Save(user).Error
		if err != nil {
			return err
		}

		return nil
	})

	if err != nil {
		return nil, err
	}
	return newFile, nil
}

func (s *Store) GetFile(ctx context.Context, id uuid.UUID) (*model.File, error) {
	file := &model.File{
		ID: id,
	}
	err := s.db.First(file).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, yerror.ErrNotFound
	} else if err != nil {
		return nil, err
	}

	if err = auth.ContextHasScopeOrIsUserIDAndHasScope(ctx, auth.ScopeAdminRead, &file.UserID, auth.ScopeSelfRead); err != nil {
		return nil, err
	}

	return file, nil
}

func (s *Store) GetFileWithBlob(ctx context.Context, publicID string) (*model.File, io.ReadSeeker, error) {
	file := &model.File{
		PublicID: publicID,
	}
	err := s.db.Where(file).First(file).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, nil, yerror.ErrNotFound
	} else if err != nil {
		return nil, nil, fmt.Errorf("failed to get file from DB: %w", err)
	}

	f, err := s.storageProvider.GetFile(ctx, hex.EncodeToString(file.Hash))
	if err != nil {
		return nil, nil, fmt.Errorf("failed to get file from storage provider: %w", err)
	}

	return file, f, nil
}

func (s *Store) DeleteFile(ctx context.Context, id uuid.UUID) error {
	file := &model.File{ID: id}
	err := s.db.First(file).Error

	if errors.Is(err, gorm.ErrRecordNotFound) {
		return yerror.ErrNotFound
	} else if err != nil {
		return err
	}

	if err = auth.ContextHasScopeOrIsUserIDAndHasScope(ctx, auth.ScopeAdminDelete, &file.UserID, auth.ScopeSelfDelete); err != nil {
		return err
	}

	err = s.db.Transaction(func(tx *gorm.DB) error {
		delRes := tx.Delete(file)
		if err = delRes.Error; err != nil {
			return err
		}

		user := &model.User{
			ID: file.UserID,
		}
		err = tx.Clauses(clause.Locking{Strength: "UPDATE"}).Find(user).Error
		if err != nil {
			return err
		}

		if file.Size > user.UsedStorage {
			user.UsedStorage = 0 // this can technically never be called, but you never know
		} else {
			user.UsedStorage -= file.Size
		}
		err = tx.Save(user).Error
		if err != nil {
			return err
		}

		err = tx.Where(&model.File{Hash: file.Hash}).First(&model.File{}).Error
		if errors.Is(err, gorm.ErrRecordNotFound) {
			// the last instance of the hash is gone, remove the actual file too
			err = s.storageProvider.DeleteFile(ctx, hex.EncodeToString(file.Hash))
		}
		if err != nil {
			return err
		}

		n := delRes.RowsAffected
		if n == 0 {
			return yerror.ErrNotFound
		}
		return nil
	})

	return err
}
