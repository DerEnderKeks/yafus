package store

import (
	"context"
	"crypto/sha256"
	"errors"
	"github.com/google/uuid"
	"gitlab.com/DerEnderKeks/yafus/internal/auth"
	"gitlab.com/DerEnderKeks/yafus/internal/model"
	"gitlab.com/DerEnderKeks/yafus/internal/yerror"
	"gitlab.com/DerEnderKeks/yafus/pkg/slice"
	"gorm.io/gorm"
)

func (s *Store) GetTokens(ctx context.Context, userID uuid.UUID) ([]*model.Token, error) {
	if err := auth.ContextHasScopeOrIsUserIDAndHasScope(ctx, auth.ScopeAdminRead, &userID, auth.ScopeSelfRead); err != nil {
		return nil, err
	}

	var tokens []*model.Token
	err := s.db.Where(&model.Token{UserID: userID}).Find(&tokens).Error
	return tokens, err
}

func (s *Store) GetToken(ctx context.Context, id uuid.UUID) (*model.Token, error) {
	token := &model.Token{
		ID: id,
	}
	err := s.db.First(token).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, yerror.ErrNotFound
	} else if err != nil {
		return nil, err
	}

	if err = auth.ContextHasScopeOrIsUserIDAndHasScope(ctx, auth.ScopeAdminRead, &token.UserID, auth.ScopeSelfRead); err != nil {
		return nil, err
	}

	return token, nil
}

func (s *Store) GetTokenWithUserByToken(ctx context.Context, token string) (*model.Token, error) {
	h := sha256.New()
	_, err := h.Write([]byte(token))
	if err != nil {
		return nil, err
	}

	dbToken := &model.Token{
		Hash: h.Sum(nil),
	}
	err = s.db.Where(dbToken).Preload("User").First(dbToken).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, yerror.ErrNotFound
	} else if err != nil {
		return nil, err
	}

	return dbToken, nil
}

func (s *Store) CreateToken(ctx context.Context, input model.TokenInput) (*model.CreatedToken, error) {
	if err := auth.ContextHasScope(ctx, auth.ScopeSelfWrite); err != nil {
		return nil, err
	}

	authCtx := auth.ForContext(ctx)

	var unauthorizedScopes []auth.Scope
	for _, newScope := range input.Scopes {
		if !slice.Contains(*authCtx.Scopes, func(scope auth.Scope) bool {
			return scope == auth.Scope(newScope)
		}) {
			unauthorizedScopes = append(unauthorizedScopes, auth.Scope(newScope))
		}
	}

	if len(unauthorizedScopes) != 0 {
		return nil, yerror.ErrForbidden.WithDetails("unauthorized scopes").AddExtensions(yerror.Extension{Key: "scopes", Value: unauthorizedScopes})
	}

	rawToken := randomID(64)
	h := sha256.New()
	_, err := h.Write([]byte(rawToken))
	if err != nil {
		return nil, err
	}

	newToken := &model.Token{
		UserID: authCtx.User.ID,
		Hash:   h.Sum(nil),
		Scopes: input.Scopes,
	}

	err = s.db.Create(newToken).Error
	if err != nil {
		return nil, err
	}

	return &model.CreatedToken{
		ID:        newToken.ID,
		CreatedAt: newToken.CreatedAt,
		UpdatedAt: newToken.UpdatedAt,
		UserID:    newToken.UserID,
		Scopes:    newToken.Scopes,
		Token:     rawToken,
	}, nil
}

func (s *Store) DeleteToken(ctx context.Context, id uuid.UUID) error {
	token := &model.Token{
		ID: id,
	}
	err := s.db.First(token).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return yerror.ErrNotFound
	} else if err != nil {
		return err
	}

	if err = auth.ContextHasScopeOrIsUserIDAndHasScope(ctx, auth.ScopeAdminDelete, &token.UserID, auth.ScopeSelfDelete); err != nil {
		return err
	}

	delRes := s.db.Delete(token)
	if err = delRes.Error; err != nil {
		return err
	}

	n := delRes.RowsAffected
	if n == 0 {
		return yerror.ErrNotFound
	}
	return nil
}
