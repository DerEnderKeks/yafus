-- +goose Up
create table files (
	id         uuid primary key default gen_random_uuid(),
	created_at timestamptz not null,
	updated_at timestamptz not null,
	user_id    uuid        not null references users (id) on delete cascade,
	public_id  text unique not null,
	name       text,
	extension  text,
	size       bigint      not null,
	mime_type  text        not null,
	hash       bytea       not null
);

create index files_hash on files (hash);

-- +goose Down
drop table files;
