-- +goose Up
create table tokens (
	id         uuid primary key default gen_random_uuid(),
	created_at timestamptz not null,
	updated_at timestamptz not null,
	user_id    uuid        not null references users (id) on delete cascade,
	hash       bytea       not null unique,
	scopes     text[]
);

-- +goose Down
drop table tokens;
