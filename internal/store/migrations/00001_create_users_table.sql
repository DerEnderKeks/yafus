-- +goose Up
create table users (
	id                 uuid primary key     default gen_random_uuid(),
	created_at         timestamptz not null,
	updated_at         timestamptz not null,
	preferred_username text,
	openid_sub         text unique not null,
	used_storage       bigint      not null default 0
);

create index users_preferred_username on users (preferred_username);

-- +goose Down
drop table users;
