package store

import (
	"embed"
	"fmt"
	"github.com/pressly/goose/v3"
	"github.com/rs/zerolog/log"
	_ "gitlab.com/DerEnderKeks/yafus/internal/store/migrations"
	"strings"
)

func init() {
	// TODO handle goose logs better (hide?)
	goose.SetLogger(gooseLogger{})
}

type gooseLogger struct{}

func (l gooseLogger) Fatal(v ...interface{}) {
	log.Fatal().Msg(strings.TrimSpace(fmt.Sprint(v...)))
}

func (l gooseLogger) Fatalf(format string, v ...interface{}) {
	log.Fatal().Msg(strings.TrimSpace(fmt.Sprintf(format, v...)))
}

func (l gooseLogger) Print(v ...interface{}) {
	log.Info().Msg(strings.TrimSpace(fmt.Sprint(v...)))
}

func (l gooseLogger) Println(v ...interface{}) {
	log.Info().Msg(strings.TrimSpace(fmt.Sprint(v...)))
}

func (l gooseLogger) Printf(format string, v ...interface{}) {
	log.Info().Msg(strings.TrimSpace(fmt.Sprintf(format, v...)))
}

const MigrateLatestVersion int64 = -1

//go:embed migrations/*.sql
var migrationsFS embed.FS

const migrationsFSSubPath = "migrations"

func (s *Store) MigrateUp() error {
	return s.MigrateUpTo(-1)
}

func (s *Store) MigrateUpTo(version int64) error {
	sql, err := s.db.DB()
	if err != nil {
		return fmt.Errorf("failed to accesss underlying connection: %v", err)
	}

	goose.SetBaseFS(migrationsFS)

	err = goose.SetDialect("postgres")
	if err != nil {
		panic(err)
	}

	msg := "migration up to "
	if version == MigrateLatestVersion {
		msg += "latest version (%d)"
		err = goose.Up(sql, migrationsFSSubPath)
	} else {
		msg += "version %d"
		err = goose.UpTo(sql, migrationsFSSubPath, version)
	}

	if err != nil {
		msg += " failed: %v"
		return fmt.Errorf(msg, version, err)
	}

	return nil
}

func (s *Store) MigrateDownTo(version int64) error {
	sql, err := s.db.DB()
	if err != nil {
		return fmt.Errorf("failed to accesss underlying connection: %v", err)
	}

	goose.SetBaseFS(migrationsFS)

	err = goose.SetDialect("postgres")
	if err != nil {
		panic(err)
	}

	err = goose.DownTo(sql, migrationsFSSubPath, version)

	if err != nil {
		return fmt.Errorf("migration down to version %d failed: %v", version, err)
	}

	return nil
}
