package store

import (
	"context"
	"errors"
	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"gitlab.com/DerEnderKeks/yafus/internal/auth"
	"gitlab.com/DerEnderKeks/yafus/internal/model"
	"gitlab.com/DerEnderKeks/yafus/internal/yerror"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func (s *Store) GetUsers(ctx context.Context) ([]*model.User, error) {
	if err := auth.ContextHasScope(ctx, auth.ScopeAdminRead); err != nil {
		return nil, err
	}

	var users []*model.User
	err := s.db.Find(&users).Error
	return users, err
}

func (s *Store) GetSelf(ctx context.Context) (*model.User, error) {
	ra := auth.ForContext(ctx)
	if ra == nil {
		return nil, yerror.ErrUnauthorised
	}
	return ra.User, nil
}

func (s *Store) GetUser(ctx context.Context, id uuid.UUID) (*model.User, error) {
	if err := auth.ContextHasScopeOrIsUserIDAndHasScope(ctx, auth.ScopeAdminRead, &id, auth.ScopeSelfRead); err != nil {
		return nil, err
	}

	user := &model.User{
		ID: id,
	}
	err := s.db.First(user).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		err = yerror.ErrNotFound
	}
	return user, err
}

func (s *Store) GetOrCreateUserBySub(ctx context.Context, sub string, nameFunc func() (*string, error)) (*model.User, error) {
	// intentionally unauthenticated, only called by auth middleware

	user := &model.User{
		OpenidSub: sub,
	}
	err := s.db.Where(user).First(user).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		log.Debug().Str("sub", sub).Msg("Creating new user")
		// fetch the username from the OIDC provider using the injected func
		// TODO also update the name at some point
		user.PreferredUsername, err = nameFunc()
		if err != nil {
			return nil, err
		}

		err = s.db.Clauses(clause.OnConflict{
			Columns:   []clause.Column{{Name: "id"}},
			DoUpdates: clause.AssignmentColumns([]string{"preferred_username"}),
		}).Create(user).Error
		if err != nil {
			return nil, err
		}

		user.PreferredUsername = nil // remove from filter
		err = s.db.Where(user).First(user).Error
		if err != nil {
			return nil, err
		}

		msg := log.Info().
			Str("sub", sub).
			Str("id", user.ID.String())

		if user.PreferredUsername != nil {
			msg.Str("preferredUsername", *user.PreferredUsername)
		}

		msg.Msg("Created new user")
	} else if err != nil {
		return nil, err
	}

	return user, nil
}

func (s *Store) DeleteUser(ctx context.Context, id uuid.UUID) error {
	if err := auth.ContextHasScopeOrIsUserIDAndHasScope(ctx, auth.ScopeAdminDelete, &id, auth.ScopeSelfDelete); err != nil {
		return err
	}

	res := s.db.Delete(model.User{ID: id})

	err := res.Error
	if err != nil {
		return err
	}

	n := res.RowsAffected
	if n == 0 {
		return yerror.ErrNotFound
	}
	return nil
}
