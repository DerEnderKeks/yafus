# Yafus

*Yet Another File Upload Server*

Need to quickly share files like screenshots with a public URL? This might be for you.

Yafus is a backend and UI, allowing you to use most tools capable of uploading files using HTTP and share the file using the returned URL. The primary use case is to upload screenshots and recordings, but any file type can be uploaded.

## Documentation

See [docs](docs). If anything is unclear or just blatantly undocumented, open an issue or merge request.

## License

See [License](License).
